package io.synapse.fs;

import org.junit.Test;

import java.util.NoSuchElementException;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.*;

public class StringReaderTest {

    @Test
    public void characterInStringCanBeRead() {
        final StringReader reader = new StringReader("foo");

        assertThat(reader.readChar()).isEqualTo('f');
        assertThat(reader.readChar()).isEqualTo('o');
        assertThat(reader.readChar()).isEqualTo('o');
        assertThatThrownBy(() -> reader.readChar()).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void hasRemainingShouldReturnTrueOnlyIfAnyCharactersAreLeft() {
        final StringReader reader = new StringReader("foo");

        assertThat(reader.hasRemaining()).isTrue();
        assertThat(reader.readChar()).isEqualTo('f');
        assertThat(reader.hasRemaining()).isTrue();
        assertThat(reader.readChar()).isEqualTo('o');
        assertThat(reader.hasRemaining()).isTrue();
        assertThat(reader.readChar()).isEqualTo('o');
        assertThat(reader.hasRemaining()).isFalse();
    }

    @Test
    public void staticStringCanBeRead() {
        final StringReader reader = new StringReader("foobar");

        assertThat(reader.read("foo")).isTrue();
        assertThat(reader.remaining()).isEqualTo("bar");
    }

    @Test
    public void staticStringLongerThanOriginalShouldBeRejected() {
        final StringReader reader = new StringReader("foobar");

        assertThat(reader.read("foobar1")).isFalse();
        assertThat(reader.read("foobar")).isTrue();
        assertThat(reader.hasRemaining()).isFalse();
    }

    @Test
    public void staticStringCannotBeReadIfReaderDoesNotContainString() {
        final StringReader reader = new StringReader(" foo");

        assertThat(reader.read("foo")).isFalse();
        assertThat(reader.read(" foo")).isTrue();
        assertThat(reader.hasRemaining()).isFalse();
    }

    @Test
    public void remainingShouldReturnRemainderOfString() {
        final StringReader reader = new StringReader("foo");

        assertThat(reader.remaining()).isEqualTo("foo");
        assertThat(reader.readChar()).isEqualTo('f');
        assertThat(reader.remaining()).isEqualTo("oo");
        assertThat(reader.readChar()).isEqualTo('o');
        assertThat(reader.remaining()).isEqualTo("o");
        assertThat(reader.readChar()).isEqualTo('o');
        assertThat(reader.remaining()).isNull();
    }

    @Test
    public void regularExpressionCanBeRead() {
        final StringReader reader = new StringReader("/foo/bar");

        assertThat(reader.read(Pattern.compile("/[^/]+"))).contains("/foo");
        assertThat(reader.remaining()).isEqualTo("/bar");
        assertThat(reader.read(Pattern.compile("/[^/]+"))).contains("/bar");
        assertThat(reader.hasRemaining()).isFalse();
    }

    @Test
    public void regularExpressionCantBeReadIfNoMatch() {
        final StringReader reader = new StringReader("FOObar");

        assertThat(reader.read(Pattern.compile("[A-Z]{4}"))).isEmpty();
        assertThat(reader.remaining()).isEqualTo("FOObar");
    }

    @Test
    public void readUntilShouldReturnCharactersUtilSeparator() {
        final StringReader reader = new StringReader("foo|bar");

        assertThat(reader.readUntil("|")).contains("foo");
        assertThat(reader.remaining()).isEqualTo("bar");
    }

    @Test
    public void readUntilShouldReturnNullIfSeparatorIsNotFound() {
        final StringReader reader = new StringReader("foo");

        assertThat(reader.readUntil("|")).isEmpty();
        assertThat(reader.remaining()).isEqualTo("foo");
    }

    @Test
    public void readUntilShouldReturnEmptyIfLookingAtSeparator() {
        final StringReader reader = new StringReader("|foo");

        assertThat(reader.readUntil("|")).contains("");
        assertThat(reader.remaining()).isEqualTo("foo");
    }

    @Test
    public void readUntilShouldWorkIfSeparatorIsAtEnd() {
        final StringReader reader = new StringReader("foo|");

        assertThat(reader.readUntil("|")).contains("foo");
        assertThat(reader.remaining()).isNull();
    }

    @Test
    public void stringCanBeReadUntilOneOfMultipleSeparators() {
        final StringReader reader = new StringReader("/foo\\bar/baz/");

        assertThat(reader.readUntil("/", "\\")).contains("");
        assertThat(reader.remaining()).isEqualTo("foo\\bar/baz/");
        assertThat(reader.readUntil("/", "\\")).contains("foo");
        assertThat(reader.readUntil("/", "\\")).contains("bar");
        assertThat(reader.remaining()).isEqualTo("baz/");
        assertThat(reader.readUntil("/", "\\")).contains("baz");
        assertThat(reader.hasRemaining()).isFalse();
    }

    @Test
    public void readAllShouldReadEveryThingAvailable() {
        final StringReader reader = new StringReader("foo");

        assertThat(reader.readAll()).contains("foo");
        assertThat(reader.readAll()).isEmpty();
    }
}