package io.synapse.fs;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.UUID;
import java.util.function.Consumer;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@SuppressWarnings("unchecked")
public class PathPatternTest {

    private final Consumer<Path> consumer = mock(Consumer.class);

    private Path directory;

    @Before
    public void setup() throws Exception {
        this.directory = Paths.get(System.getProperty("java.io.tmpdir"), "SYNAPSE-" + UUID.randomUUID().toString());

        Files.createDirectory(directory);
    }

    @After
    public void tearDown() throws Exception {
        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Test
    public void staticFilePatternCanBeCreatedForDirectory() {
        final PathPattern pattern = PathPattern.from(".");

        pattern.visit(consumer);

        verify(consumer).accept(Paths.get(System.getProperty("user.dir")));
    }

    @Test
    public void absolutePathToFileCanBeResolved() throws Exception {
        final Path file = directory.resolve("file.csv");

        Files.write(file, "foo".getBytes());

        final PathPattern pattern = PathPattern.from(directory.toString() + "/file.csv");

        pattern.visit(consumer);

        verify(consumer).accept(file);
    }

    @Test
    public void filesInDirectoryCanBeResolved() throws Exception {
        final Path file1 = directory.resolve("file1.csv");
        final Path file2 = directory.resolve("file2.csv");
        final Path file3 = directory.resolve("file3.csv");

        Files.write(file1, "foo".getBytes());
        Files.write(file2, "bar".getBytes());
        Files.write(file3, "baz".getBytes());

        final PathPattern pattern = PathPattern.from(directory.resolve("*.csv").toString());

        pattern.visit(consumer);

        verify(consumer).accept(eq(file1));
        verify(consumer).accept(eq(file2));
        verify(consumer).accept(eq(file3));
    }

    @Test
    public void filesInAnySubPathCanBeResolved() throws Exception {
        final Path subDirectory1 = Files.createDirectory(directory.resolve("sub1"));
        final Path subFile1 = Files.write(subDirectory1.resolve("file1.csv"), "x".getBytes());
        final Path subFile2 = Files.write(subDirectory1.resolve("file2.csv"), "x".getBytes());

        final Path subDirectory2 = Files.createDirectory(directory.resolve("sub2"));
        final Path subFile3 = Files.write(subDirectory2.resolve("file3.csv"), "x".getBytes());

        final Path subSubDirectory1 = Files.createDirectory(subDirectory2.resolve("subsub21"));
        final Path subFile4 = Files.write(subSubDirectory1.resolve("file4.csv"), "c".getBytes());

        PathPattern.from(directory.resolve("**/*.csv").toString()).visit(consumer);

        verify(consumer).accept(subFile1);
        verify(consumer).accept(subFile2);
        verify(consumer).accept(subFile3);
        verify(consumer).accept(subFile4);
    }

}