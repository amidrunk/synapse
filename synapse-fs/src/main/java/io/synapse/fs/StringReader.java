package io.synapse.fs;

import org.apache.commons.lang3.Validate;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringReader {

    private final CharSequence charSequence;

    private volatile int index;

    public StringReader(CharSequence charSequence) {
        this.charSequence = Validate.notNull(charSequence, "charSequence");
    }

    public char readChar() {
        if (index >= charSequence.length()) {
            throw new NoSuchElementException(index + " >= " + charSequence.length());
        }

        return charSequence.charAt(index++);
    }

    public boolean read(String staticString) {
        Validate.notEmpty(staticString, "staticString");

        if (index + staticString.length() > charSequence.length()) {
            return false;
        }

        for (int i = 0; i < staticString.length(); i++) {
            if (charSequence.charAt(index + i) != staticString.charAt(i)) {
                return false;
            }
        }

        index += staticString.length();

        return true;
    }

    public Optional<String> read(Pattern pattern) {
        Validate.notNull(pattern, "pattern");

        final Matcher matcher = pattern.matcher(charSequence).region(index, charSequence.length());

        if (!matcher.lookingAt()) {
            return Optional.empty();
        }

        final String result = matcher.group();

        index += result.length();

        return Optional.of(result);
    }

    public Optional<CharSequence> readAll() {
        if (!hasRemaining()) {
            return Optional.empty();
        }

        final CharSequence result = charSequence.subSequence(index, charSequence.length());

        index = charSequence.length();

        return Optional.of(result);
    }

    public CharSequence remaining() {
        if (index >= charSequence.length()) {
            return null;
        }

        return charSequence.subSequence(index, charSequence.length());
    }

    public boolean hasRemaining() {
        return index < charSequence.length();
    }

    public Optional<CharSequence> readUntil(String separator) {
        Validate.notEmpty(separator, "separator");

        int start = index;

        for (; start + separator.length() <= charSequence.length(); start++) {
            boolean match = true;

            for (int i = 0; i < separator.length() && match; i++) {
                match = (charSequence.charAt(start + i) == separator.charAt(i));
            }

            if (match) {
                final CharSequence result = charSequence.subSequence(index, start);

                index = start + separator.length();

                return Optional.of(result);
            }
        }

        return Optional.empty();
    }

    public Optional<CharSequence> readUntil(String ... separators) {
        Validate.notEmpty(separators, "separators");

        for (int start = index; start < charSequence.length(); start++) {
            boolean anySeparatorChecked = false;

            for (final String separator : separators) {
                if (start + separator.length() > charSequence.length()) {
                    break;
                }

                boolean match = true;

                for (int i = 0; i < separator.length() && match; i++) {
                    match = (charSequence.charAt(start + i) == separator.charAt(i));
                }

                anySeparatorChecked = true;

                if (match) {
                    final CharSequence result = charSequence.subSequence(index, start);

                    index = start + separator.length();

                    return Optional.of(result);
                }
            }

            if (!anySeparatorChecked) {
                break;
            }
        }

        return Optional.empty();
    }
}
