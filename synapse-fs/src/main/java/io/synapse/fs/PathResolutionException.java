package io.synapse.fs;

public class PathResolutionException extends RuntimeException {

    public PathResolutionException() {
    }

    public PathResolutionException(String message) {
        super(message);
    }

    public PathResolutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public PathResolutionException(Throwable cause) {
        super(cause);
    }

    public PathResolutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
