package io.synapse.fs;

import org.apache.commons.lang3.Validate;

public class ListFiles {

    private final PathPattern pattern;

    public ListFiles(PathPattern pattern) {
        this.pattern = Validate.notNull(pattern, "pattern");
    }

    public PathPattern getPattern() {
        return pattern;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ListFiles listFiles = (ListFiles) o;

        return pattern.equals(listFiles.pattern);

    }

    @Override
    public int hashCode() {
        return pattern.hashCode();
    }

    @Override
    public String toString() {
        return "ListFiles{" +
                "pattern=" + pattern +
                '}';
    }
}
