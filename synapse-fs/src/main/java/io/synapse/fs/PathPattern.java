package io.synapse.fs;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.Validate;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Iterator;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Pattern;

// TODO: Should support "/foo/**/*.csv", "/foo/*/*/*.csv" etc
public class PathPattern {

    private final ImmutableList<Node> nodes;

    private PathPattern(ImmutableList<Node> nodes) {
        this.nodes = Validate.notEmpty(nodes, "nodes");
    }

    public void visit(Consumer<Path> consumer) {
        visit(FileSystems.getDefault(), consumer);
    }

    public void visit(FileSystem fileSystem, Consumer<Path> consumer) {
        Validate.notNull(fileSystem, "fileSystem");
        Validate.notNull(consumer, "consumer");

        visit(fileSystem, null, nodes.iterator(), consumer);
    }

    private void visit(FileSystem fileSystem, Path currentPath, Iterator<Node> iterator, Consumer<Path> consumer) {
        final Node nextNode = iterator.next();
        final boolean leaf = !iterator.hasNext();

        nextNode.resolve(fileSystem, currentPath, leaf
                ? consumer
                : nextPath -> visit(fileSystem, nextPath, iterator, consumer));
    }

    public static PathPattern from(String pattern) {
        Validate.notEmpty(pattern, "pattern");

        final StringReader reader = new StringReader(pattern);
        final ImmutableList.Builder<Node> nodes = ImmutableList.builder();

        boolean first;

        if (reader.read(File.separator) || reader.read("/")) {
            nodes.add(new RootDirectory());
            first = false;
        } else {
            first = true;
        }

        while (reader.hasRemaining()) {
            Optional<CharSequence> element = reader.readUntil(File.separator, "/");

            if (!element.isPresent()) {
                element = reader.readAll();
            }

            if (!element.isPresent()) {
                break;
            }

            final CharSequence path = element.get();

            if (path.equals(".")) {
                nodes.add(new CurrentDirectory());
            } else {
                if (first) {
                    nodes.add(new CurrentDirectory());
                }

                final String fileName = path.toString();

                // check **
                if (fileName.equals("**")) {
                    nodes.add(new TreeMatcher(pattern));
                    break;
                } else if (fileName.contains("*")) {
                    final Pattern regex = Pattern.compile(fileName.replace(".", "\\.").replace("*", ".*"));

                    nodes.add(new ChildPathMatcher(regex));
                } else {
                    nodes.add(new ChildPath(fileName));
                }
            }

            first = false;
        }

        return new PathPattern(nodes.build());
    }

    public interface Node {

        void resolve(FileSystem fileSystem, Path initial, Consumer<Path> consumer);
    }

    public static class CurrentDirectory implements Node {

        @Override
        public void resolve(FileSystem fileSystem, Path initial, Consumer<Path> consumer) {
            if (initial == null) {
                consumer.accept(fileSystem.getPath(System.getProperty("user.dir")));
            } else {
                consumer.accept(initial);
            }
        }

        @Override
        public String toString() {
            return "CurrentDirectory{}";
        }
    }

    public static class RootDirectory implements Node {
        @Override
        public void resolve(FileSystem fileSystem, Path initial, Consumer<Path> consumer) {
            if (initial == null) {
                fileSystem.getRootDirectories().forEach(consumer);
            } else {
                // ?
            }
        }

        @Override
        public String toString() {
            return "RootDirectory{}";
        }
    }

    public static class ChildPath implements Node {

        private final String name;

        public ChildPath(String name) {
            this.name = Validate.notEmpty(name, "name");
        }

        @Override
        public void resolve(FileSystem fileSystem, Path initial, Consumer<Path> consumer) {
            if (initial == null) {
                throw new IllegalStateException("Child path \"" + name + "\" can't be resolved in root");
            }

            final Path path = initial.resolve(name);

            if (Files.exists(path)) {
                consumer.accept(path);
            }
        }

        @Override
        public String toString() {
            return "ChildPath{name=\"" + name + "\"}";
        }
    }

    public static class ChildPathMatcher implements Node {

        private final Pattern pattern;

        public ChildPathMatcher(Pattern pattern) {
            this.pattern = Validate.notNull(pattern, "pattern");
        }

        @Override
        public void resolve(FileSystem fileSystem, Path initial, Consumer<Path> consumer) {
            if (initial == null) {
                throw new IllegalArgumentException("Can't match child paths without initial context");
            }

            if (!Files.exists(initial)) {
                return;
            }

            final DirectoryStream<Path> ds;

            try {
                ds = Files.newDirectoryStream(initial);
            } catch (IOException e) {
                throw new PathResolutionException();
            }

            for (final Iterator<Path> i = ds.iterator(); i.hasNext(); ) {
                final Path childPath = i.next();
                final String localName = childPath.getFileName().toString();

                if (pattern.matcher(localName).matches()) {
                    consumer.accept(childPath);
                }
            }
        }
    }

    public static class TreeMatcher implements Node {

        private final String pattern;

        public TreeMatcher(String pattern) {
            this.pattern = Validate.notEmpty(pattern, "pattern");
        }

        @Override
        public void resolve(FileSystem fileSystem, Path initial, Consumer<Path> consumer) {
            if (initial == null) {
                throw new PathResolutionException("Tree matcher requires initial context");
            }

            final PathMatcher matcher = fileSystem.getPathMatcher("glob:" + pattern);

            try {
                Files.walkFileTree(initial, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if (matcher.matches(file)) {
                            consumer.accept(file);
                        }

                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                throw new PathResolutionException("Failed connect resolve matching files in tree", e);
            }
        }
    }
}
