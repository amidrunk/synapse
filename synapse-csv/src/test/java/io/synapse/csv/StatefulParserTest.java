package io.synapse.csv;

import org.junit.Test;
import org.mockito.InOrder;

import java.nio.CharBuffer;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class StatefulParserTest {

    private final Consumer consumer = mock(Consumer.class);

    private final StatefulParser machine = new StatefulParser(CsvFormat.byDefault(), consumer);

    @Test
    @SuppressWarnings("unchecked")
    public void csvRowsCanBeParsedPieceByPiece() {
        machine.write(CharBuffer.wrap("header_1,header_2,hea"));
        machine.write(CharBuffer.wrap("der_3\nval1,"));
        machine.write(CharBuffer.wrap("val2,val3\n"));

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2", "header_3"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"val1", "val2", "val3"}));
    }

    @Test
    public void quotedCellCanContainSeparator() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("\"foo,bar\",baz\n"));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo,bar", "baz"}));
    }

    @Test
    public void csvWithoutTrailingLineBreakCanBeParsed() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("foo,bar"));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo", "bar"}));
    }

    @Test
    public void csvFileCanEndWithLineBreak() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("foo,bar\n"));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo", "bar"}));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void trailingWhiteSpaceAtEndOfFileShouldBeIgnored() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("foo,bar\n     "));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo", "bar"}));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void columnCanHaveEmptyValue() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("11,21\n     "));
        machine.write(CharBuffer.wrap("21,\n     "));
        machine.write(CharBuffer.wrap(",32\n     "));
        machine.write(CharBuffer.wrap(",\n     "));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"11", "21"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"21", ""}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"", "32"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"", ""}));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void quoteCanBeEscaped(){
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("\"foo\\\"bar\",baz\n"));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo\"bar","baz"}));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void escapeCanBeEscaped() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("\"foo bar\\\\\",baz\n"));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo bar\\","baz"}));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void endOfFileShouldFailIfQuoteIsInProgress() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("\"foo "));

        assertThatThrownBy(() -> machine.endOfFile()).isInstanceOf(CsvFormatException.class);
    }

    @Test
    public void endOfFileShouldFailIfEscapeIsInProgress() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("foo,bar\\"));

        assertThatThrownBy(() -> machine.endOfFile()).isInstanceOf(CsvFormatException.class);
    }

    @Test
    public void whitespaceBeforeQuoteShouldBeIgnored() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("foo, \"bar baz\"\n"));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo", "bar baz"}));
    }

    @Test
    public void quoteInTheMiddleOfACellShouldBeIgnored() {
        machine.write(CharBuffer.wrap("header_1,header_2\n"));
        machine.write(CharBuffer.wrap("foo\",baz\n"));
        machine.endOfFile();

        final InOrder inOrder = inOrder(consumer);

        inOrder.verify(consumer).accept(aryEq(new String[]{"header_1", "header_2"}));
        inOrder.verify(consumer).accept(aryEq(new String[]{"foo\"", "baz"}));
    }

    @Test
    public void quotesInDataShouldBeParseable() throws Exception {
        machine.write(CharBuffer.wrap("\"123\",\"foo \"\"A\"\""));
    }
}