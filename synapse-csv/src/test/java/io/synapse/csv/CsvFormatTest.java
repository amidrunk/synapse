package io.synapse.csv;

import org.junit.Test;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class CsvFormatTest {

    @Test
    public void defaultCsvFormatCanBeCreated() {
        final CsvFormat format = CsvFormat.byDefault();

        assertThat(format.getSeparator()).isEqualTo(',');
        assertThat(format.getCharset()).isEqualTo(StandardCharsets.UTF_8);
        assertThat(format.getMaximumColumnCount()).isEqualTo(32);
        assertThat(format.getMaximumColumnWidth()).isEqualTo(2048);
    }

    @Test
    public void customCsvFormatCanBeCreated() {
        final Charset charset = mock(Charset.class);
        final CsvFormat format = CsvFormat.builder()
                .setCharset(charset)
                .setSeparator('|')
                .setMaximumColumnCount(1)
                .setMaximumColumnWidth(2)
                .build();

        assertThat(format.getCharset()).isEqualTo(charset);
        assertThat(format.getSeparator()).isEqualTo('|');
        assertThat(format.getMaximumColumnCount()).isEqualTo(1);
        assertThat(format.getMaximumColumnWidth()).isEqualTo(2);
    }
}