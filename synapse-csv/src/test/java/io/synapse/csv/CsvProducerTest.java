package io.synapse.csv;

import io.synapse.Message;
import io.synapse.MessageChannel;
import io.synapse.exchange.Mailbox;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import static io.synapse.MessagingSystem.builder;
import static org.assertj.core.api.Assertions.*;

public class CsvProducerTest {

    private File tempFile;

    private final Mailbox mailbox = new Mailbox();

    private final CsvProducer producer = CsvProducer.byDefault();

    @Before
    public void setup() throws Exception {
        this.tempFile = File.createTempFile("test", ".csv");
    }

    @After
    public void tearDown() throws Exception {
        if (tempFile.exists()) {
            assertThat(tempFile.delete()).isTrue();
        }
    }

    @Test
    public void plainCsvFileCanBeRead() throws Exception {
        final MessageChannel messageChannel = builder()
                .directlyTo(producer)
                .directlyTo(mailbox)
                .supervisedBy(n -> fail("no supervisory notification expected: " + n))
                .build()
                .newMessageChannel();

        writeCsv("header_1,header_2,header_3\nfoo,bar,baz\n");

        messageChannel.send(Message.of(ReadCsvFile.of(tempFile.toPath(), CsvFormat.byDefault())));

        final Message<StartOfFile> startOfFile = mailbox.collect().as(StartOfFile.class).get();
        final Message<CsvRow> header = mailbox.collect().as(CsvRow.class).get();
        final Message<CsvRow> row = mailbox.collect().as(CsvRow.class).get();
        final Message<EndOfFile> eof = mailbox.collect().as(EndOfFile.class).get();

        assertThat(startOfFile.getPayload()).isEqualTo(new StartOfFile(tempFile.toPath(), null));
        assertThat(header.getPayload()).isEqualTo(new CsvRow(new String[]{"header_1", "header_2", "header_3"}));
        assertThat(row.getPayload()).isEqualTo(new CsvRow(new String[]{"foo", "bar", "baz"}));
        assertThat(eof.getPayload()).isInstanceOf(EndOfFile.class);
    }

    @Test
    public void csvFileSpanningAcrossBuffersCanBeRead() throws Exception {
        final CsvProducer producer = CsvProducer.builder()
                .setBufferSize(8)
                .build();

        writeCsv("header_1,header_2,header_3\nfoo,bar,baz\n");

        final MessageChannel messageChannel = builder().directlyTo(producer).directlyTo(mailbox).build().newMessageChannel();

        messageChannel.send(Message.of(ReadCsvFile.of(tempFile.toPath(), CsvFormat.byDefault())));

        final Message<StartOfFile> startOfFileMessage = mailbox.collect().as(StartOfFile.class).get();
        final Message<CsvRow> header = mailbox.collect().as(CsvRow.class).get();
        final Message<CsvRow> row = mailbox.collect().as(CsvRow.class).get();

        assertThat(header.getPayload()).isEqualTo(new CsvRow(new String[]{"header_1", "header_2", "header_3"}));
        assertThat(row.getPayload()).isEqualTo(new CsvRow(new String[]{"foo", "bar", "baz"}));
    }

    @Test
    public void csvFileWithUnicodeCharacterAcrossBufferBoundaryCanBeRead() throws Exception {
        final CsvProducer producer = CsvProducer.builder()
                .setBufferSize(8)
                .build();

        writeCsv("abcdefgö,test\nfoo,bär\n");

        final MessageChannel messageChannel = builder().directlyTo(producer).directlyTo(mailbox).build().newMessageChannel();

        messageChannel.send(Message.of(ReadCsvFile.of(tempFile.toPath(), CsvFormat.byDefault())));

        final Message<StartOfFile> startOfFileMessage = mailbox.collect().as(StartOfFile.class).get();
        final Message<CsvRow> header = mailbox.collect().as(CsvRow.class).get();
        final Message<CsvRow> row = mailbox.collect().as(CsvRow.class).get();

        assertThat(header.getPayload()).isEqualTo(new CsvRow(new String[]{"abcdefgö", "test"}));
        assertThat(row.getPayload()).isEqualTo(new CsvRow(new String[]{"foo", "bär"}));
    }

    @Test
    public void csvFileWithHeaderCanBeRead() throws Exception {
        final CsvProducer producer = CsvProducer.builder()
                .setReadHeader(true)
                .build();

        writeCsv("h1,h2,h3\nv11,v12,v13\nv21,v22,v23");

        final MessageChannel messageChannel = builder().directlyTo(producer).directlyTo(mailbox).build().newMessageChannel();

        messageChannel.send(Message.of(ReadCsvFile.of(tempFile.toPath(), CsvFormat.byDefault())));

        final Message<StartOfFile> sof = mailbox.collect().as(StartOfFile.class).get();
        final Message<CsvRow> row1 = mailbox.collect().as(CsvRow.class).get();
        final Message<CsvRow> row2 = mailbox.collect().as(CsvRow.class).get();
        final Message<EndOfFile> eof = mailbox.collect().as(EndOfFile.class).get();

        assertThat(sof.getPayload()).isEqualTo(new StartOfFile(tempFile.toPath(), new CsvHeader(new String[]{"h1", "h2", "h3"})));
        assertThat(row1.getPayload()).isEqualTo(new CsvRow(new CsvHeader(new String[]{"h1", "h2", "h3"}), new String[]{"v11", "v12", "v13"}));
        assertThat(row2.getPayload()).isEqualTo(new CsvRow(new CsvHeader(new String[]{"h1", "h2", "h3"}), new String[]{"v21", "v22", "v23"}));
        assertThat(eof.getPayload()).isInstanceOf(EndOfFile.class);
    }

    @Test
    public void csvFileWithBomMarkerCanBeRead() throws Exception {
        writeCsvWithBomMarker("h1,h2,h3\nv11,v12,v13\n");

        final MessageChannel messageChannel = builder().directlyTo(producer).directlyTo(mailbox).build().newMessageChannel();

        messageChannel.send(Message.of(ReadCsvFile.of(tempFile.toPath(), CsvFormat.byDefault())));

        final Message<StartOfFile> startOfFileMessage = mailbox.collect().as(StartOfFile.class).get();
        final Message<CsvRow> row1 = mailbox.collect().as(CsvRow.class).get();
        final Message<CsvRow> row2 = mailbox.collect().as(CsvRow.class).get();

        assertThat(row1.getPayload()).isEqualTo(new CsvRow(new String[]{"h1", "h2", "h3"}));
        assertThat(row2.getPayload()).isEqualTo(new CsvRow(new String[]{"v11", "v12", "v13"}));
        assertThat(mailbox.collect().getPayload()).isInstanceOf(EndOfFile.class);
    }

    @Test
    public void csvFileWithQuotesInCellsCanBeRead() throws Exception {
        writeCsv("h1,h2,h3\nv11,\"v12\",v13");

        final MessageChannel messageChannel = builder().directlyTo(producer).directlyTo(mailbox).build().newMessageChannel();

        messageChannel.send(Message.of(ReadCsvFile.of(tempFile.toPath(), CsvFormat.byDefault())));

        final Message<StartOfFile> startOfFileMessage = mailbox.collect().as(StartOfFile.class).get();
        final Message<CsvRow> row1 = mailbox.collect().as(CsvRow.class).get();
        final Message<CsvRow> row2 = mailbox.collect().as(CsvRow.class).get();

        assertThat(row1.getPayload()).isEqualTo(new CsvRow(new String[]{"h1", "h2", "h3"}));
        assertThat(row2.getPayload()).isEqualTo(new CsvRow(new String[]{"v11", "v12", "v13"}));
        assertThat(mailbox.collect().getPayload()).isInstanceOf(EndOfFile.class);
    }

    @Test
    @Ignore
    public void conversationIdShouldBeRetainedForInitiationMessage() throws Exception {
        fail("Not implemented...");
    }

    private void writeCsvWithBomMarker(String data) {
        try (final OutputStream out = new FileOutputStream(tempFile)) {
            out.write(0xEF);
            out.write(0xBB);
            out.write(0xBF);
            out.write(data.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void writeCsv(String data) {
        try (final OutputStream out = new FileOutputStream(tempFile)) {
            out.write(data.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}