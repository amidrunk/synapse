package io.synapse.csv;

import io.synapse.Message;
import io.synapse.MessageContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class CsvSinkTest {

    private Path tempPath;

    @Before
    public void setup() throws Exception {
        tempPath = File.createTempFile("test", ".csv").toPath();
    }

    @After
    public void tearDown() throws Exception {
        Files.delete(tempPath);
    }

    @Test
    public void csvWithHeaderCanBeWritten() throws IOException {
        final CsvSink sink = CsvSink.writeTo(tempPath)
                .withFormat(CsvFormat.byDefault())
                .withHeader("foo", "bar", "baz");

        final MessageContext context = mock(MessageContext.class);

        sink.processMessage(context, Message.of(new CsvRow(new String[]{"val11", "val12", "val13"})));
        sink.processMessage(context, Message.of(new CsvRow(new String[]{"val21", "val22", "val23"})));
        sink.processMessage(context, Message.of(new CsvRow(new String[]{"val31", "val32", "val33"})));
        sink.processMessage(context, Message.of(new EndOfFile(tempPath, 1234L, 1, 2)));

        verify(context).send(eq(Message.of(new EndOfFile(tempPath, 1234L, 1, 2))));

        final List<String> lines = Files.readAllLines(tempPath);

        assertThat(lines).containsExactly(
                "\"foo\",\"bar\",\"baz\"",
                "\"val11\",\"val12\",\"val13\"",
                "\"val21\",\"val22\",\"val23\"",
                "\"val31\",\"val32\",\"val33\""
        );
    }

    @Test
    public void csvWithoutHeaderCanBeWritten() throws IOException {
        final CsvSink sink = CsvSink.writeTo(tempPath)
                .withFormat(CsvFormat.byDefault())
                .withoutHeader();

        final MessageContext context = mock(MessageContext.class);

        sink.processMessage(context, Message.of(new CsvRow(new String[]{"val11", "val12", "val13"})));
        sink.processMessage(context, Message.of(new CsvRow(new String[]{"val21", "val22", "val23"})));
        sink.processMessage(context, Message.of(new CsvRow(new String[]{"val31", "val32", "val33"})));
        sink.processMessage(context, Message.of(new EndOfFile(tempPath, 1234L, 1, 2)));

        verify(context).send(eq(Message.of(new EndOfFile(tempPath, 1234L, 1, 2))));

        final List<String> lines = Files.readAllLines(tempPath);

        assertThat(lines).containsExactly(
                "\"val11\",\"val12\",\"val13\"",
                "\"val21\",\"val22\",\"val23\"",
                "\"val31\",\"val32\",\"val33\""
        );
    }

    @Test
    public void nullCanBeWritten() throws Exception {
        final CsvSink sink = CsvSink.writeTo(tempPath)
                .withFormat(CsvFormat.byDefault())
                .withoutHeader();

        final MessageContext context = mock(MessageContext.class);

        sink.processMessage(context, Message.of(new CsvRow(new String[]{"val11", null})));
        sink.processMessage(context, Message.of(new CsvRow(new String[]{null, "val22"})));
        sink.processMessage(context, Message.of(new CsvRow(new String[]{null, null})));
        sink.processMessage(context, Message.of(new EndOfFile(tempPath, 1234L, 1, 2)));

        final List<String> lines = Files.readAllLines(tempPath);

        assertThat(lines).containsExactly(
                "\"val11\",",
                ",\"val22\"",
                ","
        );
    }

    @Test
    public void quoteInCellValueShouldBeReplaced() throws Exception {
        final CsvSink sink = CsvSink.writeTo(tempPath)
                .withFormat(CsvFormat.byDefault())
                .withoutHeader();

        final MessageContext context = mock(MessageContext.class);

        sink.processMessage(context, Message.of(new CsvRow(new String[]{"f\"oo", "bar"})));
        sink.processMessage(context, Message.of(new EndOfFile(tempPath, 1234L, 1, 2)));

        final List<String> lines = Files.readAllLines(tempPath);

        assertThat(lines).containsExactly("\"f\\\"oo\",\"bar\"");
    }

    @Test
    public void valuesCanBeWrittenAcrossBuffers() throws Exception {
        final String[] expected = new String[1000];

        for (int i = 0; i < expected.length; i++) {
            expected[i] = UUID.randomUUID().toString();
        }

        final CsvSink sink = CsvSink.writeTo(tempPath)
                .withFormat(CsvFormat.byDefault())
                .withoutHeader();

        final MessageContext context = mock(MessageContext.class);

        for (final String string : expected) {
            sink.processMessage(context, Message.of(new CsvRow(new String[]{string})));
        }

        sink.processMessage(context, Message.of(new EndOfFile(tempPath, 1234L, 1, 2)));

        final List<String> lines = Files.readAllLines(tempPath);
        final String[] expectedLines = Arrays.stream(expected).map(str -> "\"" + str + "\"").toArray(String[]::new);

        assertThat(lines).containsExactly(expectedLines);
    }
}