package io.synapse.csv;

import org.apache.commons.lang3.Validate;

import java.nio.charset.Charset;

public class CsvFormat {

    private final char separator;

    private final int maximumColumnCount;

    private final int maximumColumnWidth;

    private final Charset charset;

    private CsvFormat(char separator, int maximumColumnCount, int maximumColumnWidth, Charset charset) {
        Validate.isTrue(maximumColumnCount > 0, "at least 1 column required");
        Validate.isTrue(maximumColumnWidth > 0, "max column width must be greater than zero");
        Validate.notNull(charset, "charset can't be null");

        this.separator = separator;
        this.maximumColumnCount = maximumColumnCount;
        this.maximumColumnWidth = maximumColumnWidth;
        this.charset = charset;
    }

    public char getSeparator() {
        return separator;
    }

    public int getMaximumColumnCount() {
        return maximumColumnCount;
    }

    public int getMaximumColumnWidth() {
        return maximumColumnWidth;
    }

    public Charset getCharset() {
        return charset;
    }

    public static CsvFormat byDefault() {
        return new Builder().build();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private char separator = ',';

        private int maximumColumnCount = 32;

        private int maximumColumnWidth = 2048;

        private Charset charset = Charset.defaultCharset();

        public Builder setSeparator(char separator) {
            this.separator = separator;
            return this;
        }

        public Builder setMaximumColumnCount(int maximumColumnCount) {
            this.maximumColumnCount = maximumColumnCount;
            return this;
        }

        public Builder setMaximumColumnWidth(int maximumColumnWidth) {
            this.maximumColumnWidth = maximumColumnWidth;
            return this;
        }

        public Builder setCharset(Charset charset) {
            this.charset = charset;
            return this;
        }

        public CsvFormat build() {
            return new CsvFormat(separator, maximumColumnCount, maximumColumnWidth, charset);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CsvFormat csvFormat = (CsvFormat) o;

        if (separator != csvFormat.separator) return false;
        if (maximumColumnCount != csvFormat.maximumColumnCount) return false;
        if (maximumColumnWidth != csvFormat.maximumColumnWidth) return false;
        return charset.equals(csvFormat.charset);

    }

    @Override
    public int hashCode() {
        int result = (int) separator;
        result = 31 * result + maximumColumnCount;
        result = 31 * result + maximumColumnWidth;
        result = 31 * result + charset.hashCode();
        return result;
    }
}
