package io.synapse.csv;

import io.synapse.*;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.CharsetEncoder;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.util.stream.Collectors.joining;

public class CsvSink implements MessageExchange {

    private final FileChannel channel;

    private final CsvFormat format;

    private final CharBuffer charBuffer;

    private final ByteBuffer byteBuffer;

    private final CharsetEncoder encoder;

    private CsvSink(FileChannel channel, CsvFormat format) {
        this.channel = Validate.notNull(channel, "channel");
        this.format = Validate.notNull(format, "format");
        this.encoder = this.format.getCharset().newEncoder();
        this.charBuffer = CharBuffer.allocate((format.getMaximumColumnWidth() + 4) * format.getMaximumColumnCount()); // data + quotes + separator + line separator TODO: Write more than one line ffs
        this.byteBuffer = ByteBuffer.allocate((int) Math.ceil(format.getMaximumColumnWidth() * encoder.maxBytesPerChar()));
    }

    @Override
    public void processMessage(MessageContext context, Message<?> message) {
        if (message.getPayload() instanceof EndOfConversation) {
            try {
                channel.close();
            } catch (IOException e) {
                context.notify(Notifications.messageProcessingError(this, e));
                return;
            } finally {
                context.send(message);
            }
        } else if (message.getPayload() instanceof CsvRow) {
            final CsvRow row = (CsvRow) message.getPayload();

            try {
                write(row.getCells());
            } catch (IOException e) {
                context.notify(Notifications.messageProcessingError(this, e));
                return;
            }
        }
    }

    private void write(String[] cells) throws IOException {
        for (int i = 0; i < cells.length; i++) {
            final String cellValue = cells[i];

            if (cellValue != null) {
                charBuffer.put('\"');

                for (int j = 0; j < cellValue.length(); j++) {
                    final char c = cellValue.charAt(j);

                    switch (c) {
                        case '"':
                        case '\\':
                            charBuffer.put('\\');
                            break;
                    }

                    charBuffer.put(c);
                }

                charBuffer.put('\"');
            }

            if (i == cells.length - 1) {
                charBuffer.put('\n');
            } else {
                charBuffer.put(format.getSeparator());
            }
        }

        charBuffer.flip();

        encoder.encode(charBuffer, byteBuffer, true);

        byteBuffer.flip();

        channel.write(byteBuffer);

        encoder.reset();
        charBuffer.clear();
        byteBuffer.clear();
    }

    public static WriteToContinuation writeTo(Path path) {
        return format -> new WithFormatContinuation() {
            @Override
            public CsvSink withHeader(String... header) throws IOException {
                final CsvSink sink = withoutHeader();

                sink.write(header);

                return sink;
            }

            @Override
            public CsvSink withoutHeader() throws IOException {
                return new CsvSink(FileChannel.open(path, WRITE, CREATE), format);
            }
        };
    }

    public interface WriteToContinuation {

        WithFormatContinuation withFormat(CsvFormat format);

    }

    public interface WithFormatContinuation {

        CsvSink withHeader(String ... header) throws IOException;

        CsvSink withoutHeader() throws IOException;

    }
}
