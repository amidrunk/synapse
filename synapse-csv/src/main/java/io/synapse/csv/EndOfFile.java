package io.synapse.csv;

import io.synapse.EndOfConversation;
import org.apache.commons.lang3.Validate;

import java.nio.file.Path;

public class EndOfFile implements EndOfConversation {

    private final Path path;

    private final long totalBytes;

    private final long rowCount;

    private final long duration;

    public EndOfFile(Path path, long totalBytes, long rowCount, long duration) {
        Validate.notNull(path, "path");
        Validate.isTrue(totalBytes >= 0, "totalBytes must be positive");
        Validate.isTrue(rowCount >= 0, "rowCount must be positive");
        Validate.isTrue(duration >= 0, "duration must be positive");

        this.path = path;
        this.totalBytes = totalBytes;
        this.rowCount = rowCount;
        this.duration = duration;
    }

    public Path getPath() {
        return path;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public long getRowCount() {
        return rowCount;
    }

    public long getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EndOfFile endOfFile = (EndOfFile) o;

        if (totalBytes != endOfFile.totalBytes) return false;
        if (rowCount != endOfFile.rowCount) return false;
        if (duration != endOfFile.duration) return false;
        return path != null ? path.equals(endOfFile.path) : endOfFile.path == null;

    }

    @Override
    public int hashCode() {
        int result = path != null ? path.hashCode() : 0;
        result = 31 * result + (int) (totalBytes ^ (totalBytes >>> 32));
        result = 31 * result + (int) (rowCount ^ (rowCount >>> 32));
        result = 31 * result + (int) (duration ^ (duration >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "EndOfFile{" +
                "path=" + path +
                ", totalBytes=" + totalBytes +
                ", rowCount=" + rowCount +
                ", duration=" + duration +
                '}';
    }
}
