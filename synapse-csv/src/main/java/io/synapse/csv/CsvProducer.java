package io.synapse.csv;

import io.synapse.*;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static io.synapse.MessageMatchers.hasPayload;
import static io.synapse.MessageMatchers.match;

public class CsvProducer implements MessageExchange {

    private final MessageMatcher messageMatcher = match()
            .when(hasPayload(ReadCsvFile.class)).then((ctx, msg) -> {
                try {
                    produce(ctx, msg.getPayload().getPath(), msg.getPayload().getFormat());
                } catch (IOException e) {
                    ctx.notify(Notifications.messageProcessingError(this, e));
                }
            })
            .otherwise((ctx, msg) -> {
                ctx.notify(Notifications.notUnderstood(msg));
            });

    private final int bufferSize;

    private final boolean readHeader;

    public CsvProducer(int bufferSize, boolean readHeader) {
        Validate.isTrue(bufferSize > 0, "bufferSize must be positive");

        this.bufferSize = bufferSize;
        this.readHeader = readHeader;
    }

    @Override
    public void processMessage(MessageContext context, Message<?> message) {
        messageMatcher.match(context, message);
    }

    private void produce(MessageContext messageContext, Path path, CsvFormat format) throws IOException {
        final CsvStreamContext context = CsvStreamContext.builder()
                .setStart(System.nanoTime())
                .setChannel(AsynchronousFileChannel.open(path, StandardOpenOption.READ))
                .setParser(new StatefulParser(format, acceptRow(messageContext, path)))
                .setByteBuffer(ByteBuffer.allocate(bufferSize))
                .setCharBuffer(CharBuffer.allocate(bufferSize))
                .setDecoder(format.getCharset().newDecoder())
                .setPath(path)
                .setMessageContext(messageContext)
                .build();

        context.getChannel().read(
                context.getByteBuffer(),
                0,
                context.getByteBuffer().remaining(),
                initialCompletionHandler(context));
    }

    /**
     * Returns an initial completion handler that processes a BOM marker, if present.
     *
     * @param context The CSV stream context.
     * @return A completion handler connect be used for the first read.
     */
    private CompletionHandler<Integer, Object> initialCompletionHandler(final CsvStreamContext context) {
        final CompletionHandler<Integer, Object> targetCompletionHandler = defaultCompletionHandler(context);

        return new CompletionHandler<Integer, Object>() {
            @Override
            public void completed(Integer result, Object attachment) {
                if (result >= 3) {
                    final ByteBuffer byteBuffer = context.getByteBuffer();

                    if ((byteBuffer.get(0) & 0xFF) == 0xEF &&
                            (byteBuffer.get(1) & 0xFF) == 0xBB &&
                            (byteBuffer.get(2) & 0xFF) == 0xBF) {
                        // TODO: check correct format
                        byteBuffer.flip().position(3);
                        context.advance(3);
                        doRead(context, byteBuffer, result, (Integer) attachment, targetCompletionHandler);
                        return;
                    }
                }

                targetCompletionHandler.completed(result, attachment);
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                targetCompletionHandler.failed(exc, attachment);
            }
        };
    }

    private CompletionHandler<Integer, Object> defaultCompletionHandler(final CsvStreamContext context) {
        return new CompletionHandler<Integer, Object>() {
            @Override
            public void completed(Integer result, Object attachment) {
                final ByteBuffer byteBuffer = context.getByteBuffer();

                byteBuffer.flip();

                doRead(context, byteBuffer, result, (Integer) attachment, this);
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                context.getMessageContext().notify(Notifications.messageProcessingError(CsvProducer.this, exc));
            }
        };
    }

    private void doRead(CsvStreamContext context,
                        ByteBuffer byteBuffer,
                        Integer bytesRead,
                        Integer bytesRequested,
                        CompletionHandler<Integer, Object> readHandler) {
        final CharBuffer charBuffer = context.getCharBuffer();

        // If requested < result, we have reached eof
        final boolean eof = bytesRead < bytesRequested;

        while (byteBuffer.hasRemaining()) {
            final CoderResult coderResult = context.getDecoder().decode(byteBuffer, charBuffer, eof);

            if (coderResult.isError()) {
                for (int i = 0; i < coderResult.length(); i++) {
                    charBuffer.put('?');
                }

                byteBuffer.position(byteBuffer.position() + coderResult.length());
            } else {
                break;
            }
        }

        charBuffer.flip();

        // Advance the CSV stateful parser
        context.getParser().write(charBuffer);

        context.advance(bytesRead);

        // If a unicode character spans across buffer boundaries, it must be copied connect the
        // start so the subsequent portion can be read
        adjustBufferForNonParseableCharacter(context);

        charBuffer.clear();

        if (eof) {
            context.getParser().endOfFile();

            final EndOfFile endOfFile = new EndOfFile(
                    context.getPath(),
                    context.getOffset(),
                    context.getParser().getCurrentRow(),
                    TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - context.getStart()));

            context.getMessageContext().send(Message.of(endOfFile));
        } else {
            context.getChannel().read(byteBuffer, context.getOffset(), byteBuffer.remaining(), readHandler);
        }
    }

    private void adjustBufferForNonParseableCharacter(CsvStreamContext context) {
        final ByteBuffer byteBuffer = context.getByteBuffer();

        int i = 0;

        for (; byteBuffer.hasRemaining(); i++) {
            byteBuffer.put(i, byteBuffer.get());
        }

        byteBuffer.position(i);
        byteBuffer.limit(byteBuffer.capacity());
    }

    private Consumer<String[]> acceptRow(MessageContext messageContext, Path path) {
        return new Consumer<String[]>() {

            CsvHeader header = null;

            boolean first = true;

            @Override
            public void accept(String[] row) {
                if (first) {
                    first = false;

                    if (readHeader) {
                        header = new CsvHeader(row);
                        messageContext.send(Message.of(new StartOfFile(path, header)));
                        return;
                    } else {
                        messageContext.send(Message.of(new StartOfFile(path, null)));
                    }
                }

                messageContext.send(Message.of(new CsvRow(header, row)));
            }
        };
    }

    public static CsvProducer byDefault() {
        return builder().build();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private boolean readHeader = false;

        private int bufferSize = 1024;

        public Builder setReadHeader(boolean readHeader) {
            this.readHeader = readHeader;
            return this;
        }

        public Builder setBufferSize(int bufferSize) {
            this.bufferSize = bufferSize;
            return this;
        }

        public CsvProducer build() {
            return new CsvProducer(bufferSize, readHeader);
        }
    }

    public static class CsvStreamContext {

        private final long start;

        private final AsynchronousFileChannel channel;

        private final StatefulParser parser;

        private final ByteBuffer byteBuffer;

        private final CharBuffer charBuffer;

        private final CharsetDecoder decoder;

        private final Path path;

        private MessageContext messageContext;

        private long offset;

        private CsvStreamContext(long start, AsynchronousFileChannel channel, StatefulParser parser, ByteBuffer byteBuffer, CharBuffer charBuffer, CharsetDecoder decoder, Path path, MessageContext messageContext) {
            this.start = start;
            this.channel = channel;
            this.parser = parser;
            this.byteBuffer = byteBuffer;
            this.charBuffer = charBuffer;
            this.decoder = decoder;
            this.path = path;
            this.messageContext = messageContext;
        }

        public long getStart() {
            return start;
        }

        public AsynchronousFileChannel getChannel() {
            return channel;
        }

        public StatefulParser getParser() {
            return parser;
        }

        public ByteBuffer getByteBuffer() {
            return byteBuffer;
        }

        public CharBuffer getCharBuffer() {
            return charBuffer;
        }

        public CharsetDecoder getDecoder() {
            return decoder;
        }

        public Path getPath() {
            return path;
        }

        public MessageContext getMessageContext() {
            return messageContext;
        }

        public long getOffset() {
            return offset;
        }

        public void advance(long bytesRead) {
            offset += bytesRead;
        }

        public static Builder builder() {
            return new Builder();
        }

        public static class Builder {

            private long start;

            private AsynchronousFileChannel channel;

            private StatefulParser parser;

            private ByteBuffer byteBuffer;

            private CharBuffer charBuffer;

            private CharsetDecoder decoder;

            private Path path;

            private MessageContext messageContext;

            public Builder setStart(long start) {
                this.start = start;
                return this;
            }

            public Builder setChannel(AsynchronousFileChannel channel) {
                this.channel = channel;
                return this;
            }

            public Builder setParser(StatefulParser parser) {
                this.parser = parser;
                return this;
            }

            public Builder setByteBuffer(ByteBuffer byteBuffer) {
                this.byteBuffer = byteBuffer;
                return this;
            }

            public Builder setCharBuffer(CharBuffer charBuffer) {
                this.charBuffer = charBuffer;
                return this;
            }

            public Builder setDecoder(CharsetDecoder decoder) {
                this.decoder = decoder;
                return this;
            }

            public Builder setPath(Path path) {
                this.path = path;
                return this;
            }

            public Builder setMessageContext(MessageContext messageContext) {
                this.messageContext = messageContext;
                return this;
            }

            public CsvStreamContext build() {
                return new CsvStreamContext(start, channel, parser, byteBuffer, charBuffer, decoder, path, messageContext);
            }
        }
    }
}
