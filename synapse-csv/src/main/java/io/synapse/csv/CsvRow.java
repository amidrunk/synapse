package io.synapse.csv;

import org.apache.commons.lang3.Validate;

import java.util.Arrays;
import java.util.Optional;

public class CsvRow {

    private final CsvHeader header;

    private final String[] cells;

    public CsvRow(String ... cells) {
        this(null, cells);
    }

    public CsvRow(CsvHeader header, String[] cells) {
        this.header = header;
        this.cells = Validate.notNull(cells, "cells");
    }

    public Optional<CsvHeader> getHeader() {
        return Optional.ofNullable(header);
    }

    public String[] getCells() {
        return cells;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CsvRow csvRow = (CsvRow) o;

        if (header != null ? !header.equals(csvRow.header) : csvRow.header != null) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(cells, csvRow.cells);

    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(cells);
        return result;
    }

    @Override
    public String toString() {
        return "CsvRow{" +
                "header=" + header +
                ", cells=" + Arrays.toString(cells) +
                '}';
    }
}
