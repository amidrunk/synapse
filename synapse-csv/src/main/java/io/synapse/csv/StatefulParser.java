package io.synapse.csv;

import org.apache.commons.lang3.Validate;

import java.nio.BufferOverflowException;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.function.Consumer;

public class StatefulParser {

    private final Consumer<String[]> rowHandler;

    private final CsvFormat format;

    private final CharBuffer stateBuffer;

    private final String[] cellBuffer;

    private int currentCell = 0;

    private long currentRow;

    private boolean quoted;

    private boolean escaped;

    public StatefulParser(CsvFormat format, Consumer<String[]> rowHandler) {
        this.format = Validate.notNull(format, "format");
        this.rowHandler = Validate.notNull(rowHandler, "rowHandler");
        this.stateBuffer = CharBuffer.allocate(format.getMaximumColumnWidth());
        this.cellBuffer = new String[format.getMaximumColumnCount()];
    }

    public void write(CharBuffer charBuffer) {
        while (charBuffer.length() > 0) {
            final char c = charBuffer.get();

            if (escaped) {
                stateBuffer.put(c);
                escaped = false;
                continue;
            } else if (c == '\\') {
                escaped = true;
                continue;
            }

            if (quoted) {
                if (c == '"') {
                    quoted = false;
                } else {
                    try {
                        stateBuffer.put(c);
                    } catch (BufferOverflowException e) {
                        stateBuffer.flip();
                        throw new IllegalArgumentException("Missing end-quote around: " + stateBuffer);
                    }
                }

                continue;
            }

            if (c == format.getSeparator()) {
                stateBuffer.flip();
                cellBuffer[currentCell++] = stateBuffer.toString();
                this.stateBuffer.clear();
            } else if (c == '\n') {
                endOfLine();
            } else if (c == '"' && stateBuffer.position() == 0) {
                quoted = true;
            } else {
                if (this.stateBuffer.position() == 0 && Character.isWhitespace(c)) {
                    continue;
                }

                try {
                    this.stateBuffer.put(c);
                } catch (BufferOverflowException e) {
                    throw new CsvFormatException("Cell exceeded maximum allowed cell width: " + stateBuffer.flip().toString());
                }
            }
        }
    }

    /**
     * Should be called when the end of the file has been reached.
     */
    public void endOfFile() {
        if (quoted) {
            final String startQuote = stateBuffer.flip().toString();

            throw new CsvFormatException("Unterminated quote starting around: " + startQuote.substring(0, Math.min(startQuote.length(), 128)) + "...");
        }

        if (escaped) {
            throw new CsvFormatException("Unterminated escape at end-of-file: " + stateBuffer.flip());
        }

        endOfLine();
    }

    /**
     * Returns the current row number being read.
     *
     * @return The index of the current row that is being parsed.
     */
    public long getCurrentRow() {
        return currentRow;
    }

    /**
     * Called when a marker connect indicate end of the current line has been detected. This is typically
     * a line break or an eof.
     */
    private void endOfLine() {
        if (currentCell == 0 && stateBuffer.position() == 0) {
            return;
        }

        stateBuffer.flip();
        cellBuffer[currentCell++] = stateBuffer.toString();
        this.stateBuffer.clear();

        final String[] row = Arrays.copyOf(cellBuffer, currentCell);

        currentCell = 0;
        currentRow++;

        rowHandler.accept(row);
    }
}
