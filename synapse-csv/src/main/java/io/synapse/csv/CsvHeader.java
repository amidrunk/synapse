package io.synapse.csv;

import org.apache.commons.lang3.Validate;

import java.util.Arrays;

public class CsvHeader {

    private final String[] fields;

    public CsvHeader(String[] fields) {
        this.fields = Validate.notNull(fields, "fields");
    }

    public String[] getFields() {
        return fields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CsvHeader csvHeader = (CsvHeader) o;

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        return Arrays.equals(fields, csvHeader.fields);

    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(fields);
    }

    @Override
    public String toString() {
        return "CsvHeader{" +
                "fields=" + Arrays.toString(fields) +
                '}';
    }
}
