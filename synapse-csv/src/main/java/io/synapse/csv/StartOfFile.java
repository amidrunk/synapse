package io.synapse.csv;

import io.synapse.StartOfConversation;
import org.apache.commons.lang3.Validate;

import java.nio.file.Path;

public class StartOfFile implements StartOfConversation {

    private final Path path;

    private final CsvHeader header;

    public StartOfFile(Path path, CsvHeader header) {
        this.path = Validate.notNull(path, "path");
        this.header = header;
    }

    public Path getPath() {
        return path;
    }

    public CsvHeader getHeader() {
        return header;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StartOfFile that = (StartOfFile) o;

        if (!path.equals(that.path)) return false;
        return header != null ? header.equals(that.header) : that.header == null;

    }

    @Override
    public int hashCode() {
        int result = path.hashCode();
        result = 31 * result + (header != null ? header.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "StartOfFile{" +
                "path=" + path +
                ", header=" + header +
                '}';
    }
}
