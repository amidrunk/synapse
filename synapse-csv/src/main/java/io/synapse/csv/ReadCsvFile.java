package io.synapse.csv;

import org.apache.commons.lang3.Validate;

import java.nio.file.Path;

public class ReadCsvFile {

    private final Path path;

    private final CsvFormat format;

    public ReadCsvFile(Path path, CsvFormat format) {
        this.path = Validate.notNull(path, "path");
        this.format = Validate.notNull(format, "format");
    }

    public Path getPath() {
        return path;
    }

    public CsvFormat getFormat() {
        return format;
    }

    public static ReadCsvFile of(Path path, CsvFormat format) {
        return new ReadCsvFile(path, format);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReadCsvFile that = (ReadCsvFile) o;

        if (!path.equals(that.path)) return false;
        return format.equals(that.format);

    }

    @Override
    public int hashCode() {
        int result = path.hashCode();
        result = 31 * result + format.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ReadCsvFile{" +
                "path=" + path +
                ", format=" + format +
                '}';
    }
}
