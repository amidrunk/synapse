package io.synapse.producer;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.*;

public class TickSourceTest {

    @Test
    public void fixedTickShouldAlwaysReturnTheSameTickValue() {
        final TickSource tickSource = TickSource.fixed(10, TimeUnit.MILLISECONDS);

        assertThat(tickSource.nextTick()).isEqualTo(TimeUnit.MILLISECONDS.toNanos(10L));
        assertThat(tickSource.nextTick()).isEqualTo(TimeUnit.MILLISECONDS.toNanos(10L));
        assertThat(tickSource.nextTick()).isEqualTo(TimeUnit.MILLISECONDS.toNanos(10L));
    }
}