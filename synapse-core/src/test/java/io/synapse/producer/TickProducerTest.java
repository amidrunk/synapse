package io.synapse.producer;

import io.synapse.Message;
import io.synapse.MessageChannel;
import io.synapse.MessagingSystem;
import io.synapse.exchange.Aggregator;
import io.synapse.exchange.Batch;
import io.synapse.exchange.Mailbox;
import io.synapse.exchange.Sequencer;
import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class TickProducerTest {

    @Test
    public void ticksShouldBeSentToMessageChannel() throws Exception {
        final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

        try {
            final Mailbox mailbox = new Mailbox();
            final MessageChannel messageChannel = MessagingSystem.builder()
                    .directlyTo(Sequencer.startingAt(1))
                    .directlyTo(Aggregator.on(m -> m.getHeader().getAttribute("seqno").get().equals(11L)))
                    .directlyTo(mailbox)
                    .build()
                    .newMessageChannel();

            final TickProducer tickProducer = new TickProducer.Builder()
                    .setTickSource(TickSource.fixed(10, TimeUnit.MILLISECONDS))
                    .setDestination(messageChannel)
                    .setScheduler(scheduler)
                    .build();

            final long start = System.nanoTime();

            tickProducer.startAsync();

            final Message<?> message = mailbox.collect();

            tickProducer.stopAsync();

            assertThat(message.getPayload()).isInstanceOf(Batch.class);

            final Batch batch = message.as(Batch.class).get().getPayload();

            assertThat(batch.size()).isEqualTo(10);
            assertThat(TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start) >= 100);

            for (int i = 1; i < batch.size(); i++) {
                final Tick previousTick = (Tick) batch.get(i - 1);
                final Tick currentTick = (Tick) batch.get(i);
                final long durationMillis = TimeUnit.NANOSECONDS.toMillis(currentTick.nanoTime() - previousTick.nanoTime());

                assertThat(durationMillis >= 10);
            }
        } finally {
            scheduler.shutdown();
        }
    }
}