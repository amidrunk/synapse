package io.synapse.exchange;

import io.synapse.Message;
import io.synapse.MessageChannel;
import io.synapse.MessageContext;
import org.junit.Test;

import java.util.Arrays;
import java.util.function.Predicate;

import static io.synapse.MessagingSystem.builder;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class AggregatorTest {

    private final Predicate predicate = mock(Predicate.class);

    private final Aggregator aggregator = new Aggregator(predicate);

    @Test
    @SuppressWarnings("unchecked")
    public void batchOfAllMessagesPrecedingCompletionShouldBeSent() {
        final MessageContext messageContext = mock(MessageContext.class);

        doAnswer(i -> ((Message) i.getArguments()[0]).getPayload().equals("done"))
                .when(predicate)
                .test(any());

        aggregator.processMessage(messageContext, Message.of("foo"));
        aggregator.processMessage(messageContext, Message.of("bar"));
        aggregator.processMessage(messageContext, Message.of("baz"));
        aggregator.processMessage(messageContext, Message.of("done"));

        verify(messageContext).send(Message.of(Batch.of(Arrays.asList("foo", "bar", "baz"))));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void aggregationCanBePerformedInMessagingSystem() throws Exception {
        final Mailbox mailbox = new Mailbox();
        final MessageChannel messageChannel = builder()
                .directlyTo(aggregator)
                .directlyTo(mailbox)
                .build()
                .newMessageChannel();

        doAnswer(i -> ((Message) i.getArguments()[0]).getPayload().equals("done"))
                .when(predicate)
                .test(any());

        messageChannel.send(Message.of("foo"));
        messageChannel.send(Message.of("bar"));
        messageChannel.send(Message.of("baz"));
        messageChannel.send(Message.of("done"));

        assertThat(mailbox.collect()).isEqualTo(Message.of(Batch.of(Arrays.asList("foo", "bar", "baz"))));
    }
}