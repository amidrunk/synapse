package io.synapse.exchange;

import io.synapse.Header;
import io.synapse.Message;
import io.synapse.MessageContext;
import org.junit.Test;
import org.mockito.InOrder;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SequencerTest {

    @Test
    public void messageShouldBeForwardedWithSequenceNumber() {
        final Sequencer sequencer = Sequencer.startingAt(1L);
        final MessageContext messageContext = mock(MessageContext.class);

        sequencer.processMessage(messageContext, Message.of("foo"));
        sequencer.processMessage(messageContext, Message.of("bar"));
        sequencer.processMessage(messageContext, Message.of("baz"));

        final InOrder inOrder = inOrder(messageContext);

        inOrder.verify(messageContext).send(eq(Message.of("foo", Header.of("seqno", 1L))));
        inOrder.verify(messageContext).send(eq(Message.of("bar", Header.of("seqno", 2L))));
        inOrder.verify(messageContext).send(eq(Message.of("baz", Header.of("seqno", 3L))));
    }

}