package io.synapse.exchange;

import io.synapse.Message;
import io.synapse.MessageContext;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;

public class MailboxTest {

    private final Mailbox mailbox = new Mailbox();

    @Test
    public void existingMessageCanBeCollected() throws InterruptedException {
        mailbox.processMessage(mock(MessageContext.class), Message.of("foo"));

        final Message<?> result = mailbox.collect();

        assertThat(result).isEqualTo(Message.of("foo"));
    }

    @Test
    public void asynchronouslyProducedMessageCanBeCollected() throws InterruptedException {
        final Mailbox mailbox = new Mailbox();

        new Thread(() -> {
            for (int i = 0; i < 3; i++) {
                mailbox.processMessage(mock(MessageContext.class), Message.of("message_" + (i + 1)));

                try {
                    Thread.sleep(20);
                } catch (InterruptedException ignored) {
                    Thread.interrupted();
                }
            }
        }).start();

        assertThat(mailbox.collect()).isEqualTo(Message.of("message_1"));
        assertThat(mailbox.collect()).isEqualTo(Message.of("message_2"));
        assertThat(mailbox.collect()).isEqualTo(Message.of("message_3"));
    }
}