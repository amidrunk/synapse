package io.synapse;

import io.synapse.exchange.Aggregator;
import io.synapse.exchange.Batch;
import io.synapse.exchange.Mailbox;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;

import static io.synapse.MessagingSystem.builder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.not;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class MessagingSystemTest {

    @Test
    public void simpleTransformingMessagingSystemCanBeCreated() {
        final LinkedList<String> result = new LinkedList<>();

        final MessageChannel channel = MessagingSystem.builder()
                .directlyTo((ctx, m) -> ctx.send(Message.of(m.as(String.class).get().getPayload().toUpperCase())))
                .directlyTo((ctx, m) -> result.add(m.as(String.class).get().getPayload()))
                .build()
                .newMessageChannel();

        channel.send(Message.of("foo"));
        channel.send(Message.of("bar"));
        channel.send(Message.of("baz"));

        assertThat(result).containsExactly("FOO", "BAR", "BAZ");
    }

    @Test
    public void supervisorCanReceiveNotificationAboutMessageNotUnderstood() {
        final Supervisor supervisor = mock(Supervisor.class);

        final MessageChannel channel = MessagingSystem.builder()
                .directlyTo((ctx, m) -> ctx.notify(Notifications.notUnderstood(m)))
                .supervisedBy(supervisor)
                .build()
                .newMessageChannel();

        channel.send(Message.of("foo"));

        final ArgumentCaptor<Notification> captor = ArgumentCaptor.forClass(Notification.class);

        verify(supervisor).onSupervisoryNotification(captor.capture());

        final Notification notification = captor.getValue();

        assertThat(notification).isInstanceOf(NotUnderstoodNotification.class);
        assertThat(((NotUnderstoodNotification) notification).getMessage().getPayload()).isEqualTo("foo");
    }

    @Test
    public void messageSentFromSinkShouldReachSupervisor() {
        final Supervisor supervisor = mock(Supervisor.class);
        final MessageChannel messageChannel = MessagingSystem.builder()
                .directlyTo((ctx, m) -> ctx.send(m))
                .supervisedBy(supervisor)
                .build()
                .newMessageChannel();

        messageChannel.send(Message.of("foo"));

        final ArgumentCaptor<Notification> captor = ArgumentCaptor.forClass(Notification.class);
        verify(supervisor).onSupervisoryNotification(captor.capture());

        final Notification notification = captor.getValue();

        assertThat(notification).isInstanceOf(MessageNotDeliverableNotification.class);
        assertThat(((MessageNotDeliverableNotification) notification).getMessage().getPayload()).isEqualTo("foo");
    }

    @Test
    public void messageCanBeRoutedByType() throws Exception {
        final Mailbox stringMailbox = new Mailbox();
        final Mailbox integerMailbox = new Mailbox();

        final MessageChannel messageChannel = MessagingSystem.builder()
                .route()
                .when(m -> m.getPayload() instanceof String).directlyTo(stringMailbox)
                .when(m -> m.getPayload() instanceof Integer).directlyTo(integerMailbox)
                .end()
                .build()
                .newMessageChannel();

        messageChannel.send(Message.of("foo"));
        messageChannel.send(Message.of(1234));

        final Message<?> stringMessage = stringMailbox.collect();
        assertThat(stringMessage.getPayload()).isEqualTo("foo");

        final Message<?> integrationMessage = integerMailbox.collect();
        assertThat(integrationMessage.getPayload()).isEqualTo(1234);
    }

    @Test
    public void asynchronousMessageSystemCanBeCreated() throws Exception {
        final Mailbox mailbox = new Mailbox();
        final MessageExchange forwardWithDelay = (ctx, m) -> {
            try {
                Thread.sleep(10);
            } catch (InterruptedException ignored) {
                Thread.interrupted();
            }

            ctx.send(m);
        };

        try (final MessagingSystem messagingSystem = MessagingSystem.builder()
                .asyncTo(mailbox)
                .directlyTo(forwardWithDelay)
                .build()) {
            final MessageChannel channel = messagingSystem.newMessageChannel();

            channel.send(Message.of("foo"));
            channel.send(Message.of("bar"));
            channel.send(Message.of("baz"));

            assertThat(mailbox.collect().getPayload()).isEqualTo("foo");
            assertThat(mailbox.collect().getPayload()).isEqualTo("bar");
            assertThat(mailbox.collect().getPayload()).isEqualTo("baz");
        }
    }

    @Test
    public void messagesCanBeAggregatedOnPredicate() throws Exception {
        final Mailbox mailbox = new Mailbox();
        final MessageChannel messageChannel = MessagingSystem.builder()
                .directlyTo(Aggregator.on(m -> m.getPayload().equals("done")))
                .directlyTo(mailbox)
                .build()
                .newMessageChannel();

        messageChannel.send(Message.of("foo"));
        messageChannel.send(Message.of("bar"));
        messageChannel.send(Message.of("baz"));
        messageChannel.send(Message.of("done"));

        assertThat(mailbox.collect().getPayload()).isEqualTo(Batch.of(Arrays.asList("foo", "bar", "baz")));
    }

    @Test
    public void fanoutMessageSystemCanBeBuilt() throws InterruptedException {
        final Mailbox exchange1 = new Mailbox();
        final Mailbox exchange2 = new Mailbox();

        final MessageChannel channel = MessagingSystem.builder()
                .fanout()
                .directlyTo(exchange1)
                .directlyTo(exchange2)
                .end()
                .build()
                .newMessageChannel();

        channel.send(Message.of("foo"));

        final Message<?> message1 = exchange1.collect();
        final Message<?> message2 = exchange2.collect();

        assertThat(message1.getPayload()).isEqualTo("foo");
        assertThat(message2.getPayload()).isEqualTo("foo");
        assertThat(message1.getConversationId()).isEqualTo(message2.getConversationId());
    }

    @Test
    public void messageShouldBeAssignedAConversationId() throws Exception {
        final Mailbox mailbox = new Mailbox();

        final MessageChannel messageChannel = MessagingSystem.builder()
                .directlyTo((ctx, m) -> {
                    ctx.send(m);
                    ctx.send(m.continuation("second"));
                })
                .directlyTo(mailbox)
                .build()
                .newMessageChannel();

        messageChannel.send(Message.of("first"));

        final Message<?> firstMessage = mailbox.collect();
        final Message<?> secondMessage = mailbox.collect();

        final String conversationId1 = firstMessage.getConversationId().get();
        final String conversationId2 = secondMessage.getConversationId().get();

        assertThat(conversationId1).isNotNull();
        assertThat(conversationId1).isEqualTo(conversationId2);
        assertThat(firstMessage.getPayload()).isEqualTo("first");
        assertThat(secondMessage.getPayload()).isEqualTo("second");
    }

    @Test
    public void messageCanBeDistributedViaExecutor() {
        final Executor executor = mock(Executor.class);
        final MessageExchange messageExchange = mock(MessageExchange.class);

        try (final MessagingSystem ms = MessagingSystem.builder()
                .on(executor).to(messageExchange)
                .build()) {
            final MessageChannel mc = ms.newMessageChannel();

            mc.send(Message.of("foo"));
        }

        final ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);

        verify(executor).execute(runnableCaptor.capture());
        verifyZeroInteractions(messageExchange);

        runnableCaptor.getValue().run();

        final ArgumentCaptor<Message> messageCaptor = ArgumentCaptor.forClass(Message.class);

        verify(messageExchange).processMessage(any(), messageCaptor.capture());

        final Message message = messageCaptor.getValue();

        assertThat(message.getPayload()).isEqualTo("foo");
    }
}