package io.synapse;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class MessageTest {

    @Test
    public void messageWithPayloadAndHeaderCanBeBuilt() {
        final Message<String> message = Message.of("foo", Header.of("bar", "baz"));

        assertThat(message.getPayload()).isEqualTo("foo");
        assertThat(message.getHeader().getAttributes()).containsOnly(entry("bar", "baz"));
    }

    @Test
    public void messageWithoutHeaderCanBeBuilt() {
        final Message<String> message = Message.of("foo");

        assertThat(message.getPayload()).isEqualTo("foo");
        assertThat(message.getHeader()).isEqualTo(Header.empty());
    }

    @Test
    public void messageWithNullPayloadCanBeBuilt() {
        final Message<String> message = Message.of(null);

        assertThat(message.getPayload()).isNull();
        assertThat(message.getHeader().getAttributes()).isEmpty();
    }

    @Test
    public void messageCanBeCoercedToSupportingType() {
        final Message<?> message1 = Message.of("foo");

        assertThat(message1.as(String.class)).contains(Message.of("foo"));
    }

    @Test
    public void messageCannotBeCoercedToNonCompatibleType() {
        final Message<?> message = Message.of("foo");

        assertThat(message.as(Integer.class)).isEmpty();
    }
}