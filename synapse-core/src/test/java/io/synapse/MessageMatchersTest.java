package io.synapse;

import org.junit.Test;
import org.mockito.InOrder;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import static io.synapse.MessageMatchers.hasPayload;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

public class MessageMatchersTest {

    @Test
    @SuppressWarnings("unchecked")
    public void messageCanBeMatched() {
        final BiConsumer<MessageContext, Message<String>> stringConsumer = mock(BiConsumer.class);
        final BiConsumer<MessageContext, Message<Integer>> integerConsumer = mock(BiConsumer.class);
        final BiConsumer<MessageContext, Message<?>> otherConsumer = mock(BiConsumer.class);

        final MessageMatcher messageMatcher = MessageMatchers.match()
                .when(hasPayload(String.class)).then(stringConsumer)
                .when(hasPayload(Integer.class)).then(integerConsumer)
                .otherwise(otherConsumer);

        final MessageContext ctx = mock(MessageContext.class);

        messageMatcher.match(ctx, Message.of("foo"));
        messageMatcher.match(ctx, Message.of(1));
        messageMatcher.match(ctx, Message.of(1.1));

        final InOrder inOrder = inOrder(stringConsumer, integerConsumer, otherConsumer);

        inOrder.verify(stringConsumer).accept(ctx, Message.of("foo"));
        inOrder.verify(integerConsumer).accept(ctx, Message.of(1));
        inOrder.verify(otherConsumer).accept(ctx, Message.of(1.1));
        inOrder.verifyNoMoreInteractions();

        verifyZeroInteractions(otherConsumer);
    }

}