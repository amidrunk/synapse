package io.synapse.concurrent;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class WorkStealingExecutionQueueTest {

    private final WorkStealingExecutionQueue queue = new WorkStealingExecutionQueue();

    @Test
    public void enqueuedRunnablesShouldBeExecutedImmediately() {
        final Runnable runnable1 = mock(Runnable.class);
        final Runnable runnable2 = mock(Runnable.class);

        queue.execute(runnable1);
        queue.execute(runnable2);

        verify(runnable1).run();
        verify(runnable2).run();
    }

    @Test
    public void asynchronousExecutionsShouldBeExecutedExactlyOnce() throws Exception {
        final ExecutorService executorService = Executors.newFixedThreadPool(8);
        try {
            final int expectedExecutionCount = 10000;
            final AtomicInteger actualExecutionCount = new AtomicInteger(0);

            for (int i = 0; i < expectedExecutionCount; i++) {
                executorService.submit(() ->
                        queue.execute(() -> {
                            final int val = actualExecutionCount.incrementAndGet();

                            if (val == expectedExecutionCount) {
                                synchronized (actualExecutionCount) {
                                    actualExecutionCount.notifyAll();
                                }
                            }

                            // induce contention
                            final double rnd = Math.random();

                            if (rnd < .01) {
                                try {
                                    Thread.sleep(5);
                                } catch (InterruptedException e) {
                                }
                            } else if (rnd < .5) {
                                Thread.yield();
                            }
                        })
                );
            }

            synchronized (actualExecutionCount) {
                while (actualExecutionCount.get() < expectedExecutionCount) {
                    actualExecutionCount.wait(5000);
                }
            }

            assertThat(actualExecutionCount.get()).isEqualTo(expectedExecutionCount);
        } finally {
            executorService.shutdown();
        }
    }
}