package io.synapse;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class HeaderTest {

    @Test
    public void headerWithAttributesCanBeBuilt() {
        final Header header = Header.builder()
                .with("attr_1", "val_1")
                .with("attr_2", "val_2")
                .build();

        assertThat(header.getAttributes()).containsOnly(entry("attr_1", "val_1"), entry("attr_2", "val_2"));
    }

    @Test
    public void emptyHeaderCanBeCreated() {
        final Header header = Header.empty();

        assertThat(header.getAttributes()).isEmpty();
    }

    @Test
    public void headerAttributeCanBeRetrievedByName() {
        final Header header = Header.builder()
                .with("foo", "bar")
                .build();

        assertThat(header.getAttribute("foo")).contains("bar");
    }

    @Test
    public void getAttributeShouldReturnEmptyIfAttributeDoesNotExist() {
        final Header header = Header.builder()
                .with("foo", "bar")
                .build();

        assertThat(header.getAttribute("bar")).isEmpty();
    }

    @Test
    public void headerWithSingleEntryCanBeBuilt() {
        final Header header = Header.of("foo", "bar");

        assertThat(header.getAttribute("foo")).contains("bar");
    }

    @Test
    public void withHeaderShouldReturnCopyWithAddedHeader() {
        final Header header1 = Header.of("foo", "bar");
        final Header header2 = header1.withAttribute("bar", "baz");

        assertThat(header1.getAttributes()).containsOnly(entry("foo", "bar"));
        assertThat(header2.getAttributes()).contains(entry("foo", "bar"), entry("bar", "baz"));
    }

    @Test
    public void withHeaderShouldReturnCopyWithOverriddenHeaderValue(){
        final Header header1 = Header.of("foo", "bar");
        final Header header2 = header1.withAttribute("foo", "baz");

        assertThat(header1.getAttributes()).containsOnly(entry("foo", "bar"));
        assertThat(header2.getAttributes()).containsOnly(entry("foo", "baz"));
    }

    @Test
    public void headerWithTwoAttributesCanBeCreated() {
        final Header header = Header.of(
                "foo", "bar",
                "bar", "baz"
        );

        assertThat(header.getAttributes()).containsOnly(entry("foo", "bar"), entry("bar", "baz"));
    }

    @Test
    public void withoutHeaderShouldReturnCopyWithoutHeader() {
        final Header header1 = Header.of("foo", "bar", "bar", "baz");
        final Header header2 = header1.withoutAttribute("foo");
        final Header header3 = header1.withoutAttribute("bar");

        assertThat(header1.getAttributes()).containsOnly(entry("foo", "bar"), entry("bar", "baz"));
        assertThat(header2.getAttributes()).containsOnly(entry("bar", "baz"));
        assertThat(header3.getAttributes()).containsOnly(entry("foo", "bar"));
    }
}