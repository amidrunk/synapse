package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.exchange.Mailbox;
import org.junit.After;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.mock;

public class AsyncMessageDispatcherTest {

    private final Mailbox mailbox = new Mailbox();

    private final MessageContext messageContext = mock(MessageContext.class);

    private final AsyncMessageDispatcher messageDispatcher = AsyncMessageDispatcher.start(mailbox);

    @After
    public void tearDown() {
        messageDispatcher.close();
    }

    @Test
    public void messagesShouldBeDispatched() throws InterruptedException {
        messageDispatcher.dispatch(messageContext, Message.of("foo"));
        messageDispatcher.dispatch(messageContext, Message.of("bar"));
        messageDispatcher.dispatch(messageContext, Message.of("baz"));

        assertThat(mailbox.collect()).isEqualTo(Message.of("foo"));
        assertThat(mailbox.collect()).isEqualTo(Message.of("bar"));
        assertThat(mailbox.collect()).isEqualTo(Message.of("baz"));
    }

    @Test
    public void messageCannotBeDispatchedOnceDispatcherHasBeenClosed() throws InterruptedException {
        messageDispatcher.dispatch(messageContext, Message.of("foo"));

        messageDispatcher.close();

        assertThatThrownBy(() -> messageDispatcher.dispatch(messageContext, Message.of("bar"))).isInstanceOf(IllegalStateException.class);

        assertThat(mailbox.collect()).isEqualTo(Message.of("foo"));
        assertThat(mailbox.isEmpty()).isTrue();
    }
}