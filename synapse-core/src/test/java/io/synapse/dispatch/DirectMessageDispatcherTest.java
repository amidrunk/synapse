package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageExchange;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class DirectMessageDispatcherTest {

    @Test
    public void messageShouldBeDeliveredDirectlyToMessageExchange() {
        final MessageContext context = mock(MessageContext.class);
        final MessageExchange exchange = mock(MessageExchange.class);
        final DirectMessageDispatcher dispatcher = new DirectMessageDispatcher(exchange);

        dispatcher.dispatch(context, Message.of("foo"));

        verify(exchange).processMessage(eq(context), eq(Message.of("foo")));
    }
}