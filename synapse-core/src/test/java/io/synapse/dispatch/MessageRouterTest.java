package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageDispatcher;
import io.synapse.Notifications;
import org.junit.Test;
import org.mockito.InOrder;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class MessageRouterTest {

    @Test
    public void supportedMessagesCanBeDeliveredToRoutes() {
        final Message<?> message1 = Message.of("msg1");
        final Message<?> message2 = Message.of("msg2");
        final MessageDispatcher messageDispatcher1 = mock(MessageDispatcher.class);
        final MessageDispatcher messageDispatcher2 = mock(MessageDispatcher.class);
        final MessageRouter router = new MessageRouter.Builder()
                .addRoute(message -> message == message1, messageDispatcher1)
                .addRoute(message -> message == message2, messageDispatcher2)
                .build();

        final MessageContext messageContext1 = mock(MessageContext.class);
        final MessageContext messageContext2 = mock(MessageContext.class);

        router.dispatch(messageContext1, message1);
        router.dispatch(messageContext2, message2);

        final InOrder inOrder = inOrder(messageDispatcher1, messageDispatcher2);

        inOrder.verify(messageDispatcher1).dispatch(eq(messageContext1), eq(message1));
        inOrder.verify(messageDispatcher2).dispatch(eq(messageContext2), eq(message2));
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void unroutableMessageShouldBeUndeliverable() {
        final MessageRouter router = new MessageRouter.Builder().build();
        final MessageContext context = mock(MessageContext.class);

        router.dispatch(context, Message.of("foo"));

        verify(context).notify(eq(Notifications.undeliverable(Message.of("foo"))));
    }
}