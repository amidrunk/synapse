package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageExchange;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.concurrent.Executor;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

public class ExecutorMessageDispatcherTest {

    private final Executor executor = mock(Executor.class);

    private final MessageExchange messageExchange = mock(MessageExchange.class);

    private final ExecutorMessageDispatcher dispatcher = new ExecutorMessageDispatcher(executor, messageExchange);

    @Test
    public void messageShouldBeDispatchedThroughExecutor() {
        final MessageContext ctx = mock(MessageContext.class);
        final Message<String> msg = Message.of("foo");

        dispatcher.dispatch(ctx, msg);

        final ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);

        verify(executor).execute(captor.capture());

        verifyZeroInteractions(messageExchange);

        captor.getValue().run();

        verify(messageExchange).processMessage(eq(ctx), eq(msg));
    }

}