package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageDispatcher;
import io.synapse.MessageExchange;
import org.junit.Test;
import org.mockito.InOrder;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;

public class FanOutMessageDispatcherTest {

    @Test
    public void messageShouldBeDispatchedToAllExchanges() {
        final MessageDispatcher dispatcher1 = mock(MessageDispatcher.class);
        final MessageDispatcher dispatcher2 = mock(MessageDispatcher.class);
        final FanOutMessageDispatcher dispatcher = new FanOutMessageDispatcher(Arrays.asList(dispatcher1, dispatcher2));
        final MessageContext context = mock(MessageContext.class);

        dispatcher.dispatch(context, Message.of("foo"));

        final InOrder inOrder = inOrder(dispatcher1, dispatcher2);

        inOrder.verify(dispatcher1).dispatch(eq(context), eq(Message.of("foo")));
        inOrder.verify(dispatcher2).dispatch(eq(context), eq(Message.of("foo")));
    }

    @Test
    public void closeShouldCloseAllDispatchers(){
        final MessageDispatcher dispatcher1 = mock(MessageDispatcher.class);
        final MessageDispatcher dispatcher2 = mock(MessageDispatcher.class);
        final FanOutMessageDispatcher dispatcher = new FanOutMessageDispatcher(Arrays.asList(dispatcher1, dispatcher2));

        dispatcher.close();

        final InOrder inOrder = inOrder(dispatcher1, dispatcher2);

        inOrder.verify(dispatcher1).close();
        inOrder.verify(dispatcher2).close();
    }
}