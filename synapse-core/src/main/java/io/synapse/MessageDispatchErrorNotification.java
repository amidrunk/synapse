package io.synapse;

import org.apache.commons.lang3.Validate;

public class MessageDispatchErrorNotification implements MessagingErrorNotification<MessageDispatcher> {

    private final MessageDispatcher source;

    private final Throwable throwable;

    public MessageDispatchErrorNotification(MessageDispatcher source, Throwable throwable) {
        this.source = Validate.notNull(source, "source");
        this.throwable = Validate.notNull(throwable, "throwable");
    }

    @Override
    public MessageDispatcher getSource() {
        return source;
    }

    @Override
    public Throwable getThrowable() {
        return throwable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageDispatchErrorNotification that = (MessageDispatchErrorNotification) o;

        if (!source.equals(that.source)) return false;
        return throwable.equals(that.throwable);

    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + throwable.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "MessageDispatchErrorNotification{" +
                "source=" + source +
                ", throwable=" + throwable +
                '}';
    }
}
