package io.synapse.concurrent;

import org.apache.commons.lang3.Validate;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class WorkStealingExecutionQueue implements ExecutionQueue {

    private final ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>();

    private final AtomicBoolean ticket = new AtomicBoolean();

    @Override
    public void execute(Runnable runnable) {
        Validate.notNull(runnable, "runnable");

        queue.offer(runnable);

        if (ticket.compareAndSet(false, true)) {
            for (;;) {
                try {
                    for (Runnable r; (r = queue.poll()) != null; ) {
                        r.run();
                    }
                } finally {
                    ticket.set(false);
                }

                if (queue.peek() == null || !ticket.compareAndSet(false, true)) {
                    break;
                }
            }
        }
    }
}
