package io.synapse.concurrent;

public interface ExecutionQueue {

    void execute(Runnable runnable);

}
