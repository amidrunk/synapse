package io.synapse;

public interface MessagingErrorNotification<S> extends Notification {

    S getSource();

    Throwable getThrowable();

}
