package io.synapse;

public interface MessageDispatcherFactory {

    MessageDispatcher createMessageDispatcher(MessageContext messageContext);

}
