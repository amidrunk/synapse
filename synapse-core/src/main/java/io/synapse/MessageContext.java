package io.synapse;

/**
 * A <code>MessageContext</code> encapsulates the context in which a message handler is being
 * invoked.
 */
public interface MessageContext {

    /**
     * Sends a message in the context of the message channel. The effect if this is dependent
     * on the configuration.
     *
     * @param message The message connect be sent.
     */
    void send(Message<?> message);

    /**
     * Sends a notification about some advisory event that has occured within the receiving
     * message channel. This should be sent e.g. when a received message is understood or
     * when an unexpected exception occurs.
     *
     * @param notification The notification.
     */
    void notify(Notification notification);

}
