package io.synapse;

/**
 * A <code>MessageChannel</code> is the entry-point towards a messaging builder.
 */
public interface MessageChannel {

    void send(Message<?> message);

}
