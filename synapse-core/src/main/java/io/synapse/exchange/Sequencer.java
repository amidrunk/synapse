package io.synapse.exchange;

import io.synapse.Header;
import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageExchange;
import org.apache.commons.lang3.Validate;

import java.util.concurrent.atomic.AtomicLong;

/**
 * A <code>Sequencer</code> adds a sequence number to a message.
 */
public class Sequencer implements MessageExchange {

    private static final String DEFAULT_SEQUENCE_NUMBER_ATTRIBUTE_NAME = "seqno";

    private final long startSequenceNumber;

    private final AtomicLong sequenceNumber;

    private final String attributeName;

    public Sequencer(String attributeName, long startSequenceNumber) {
        this.attributeName = Validate.notEmpty(attributeName, "attributeName");
        this.startSequenceNumber = startSequenceNumber;
        this.sequenceNumber = new AtomicLong(startSequenceNumber);
    }

    @Override
    public void processMessage(MessageContext context, Message<?> message) {
        final Header newHeader = message.getHeader()
                .withAttribute(attributeName, sequenceNumber.getAndIncrement());

        context.send(Message.of(message.getPayload(), newHeader));
    }

    public static Sequencer startingAt(long startSequenceNumber) {
        return new Sequencer(DEFAULT_SEQUENCE_NUMBER_ATTRIBUTE_NAME, startSequenceNumber);
    }

    public Sequencer withAttributeName(String attributeName) {
        return new Sequencer(attributeName, startSequenceNumber);
    }
}
