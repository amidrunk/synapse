package io.synapse.exchange;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageExchange;
import io.synapse.concurrent.WorkStealingExecutionQueue;

import java.util.concurrent.*;

public class Mailbox implements MessageExchange {

    private final LinkedBlockingQueue<Message<?>> messages = new LinkedBlockingQueue<>();

    private final ConcurrentLinkedQueue<CompletableFuture<Message<?>>> futures = new ConcurrentLinkedQueue<>();

    private final WorkStealingExecutionQueue executionQueue = new WorkStealingExecutionQueue();

    @Override
    public void processMessage(MessageContext context, Message<?> message) {
        executionQueue.execute(() -> {
            final CompletableFuture<Message<?>> future = futures.poll();

            if (future != null) {
                future.complete(message);
            } else {
                messages.add(message);
            }
        });
    }

    public Message<?> collect() throws InterruptedException {
        try {
            return collectAsync().get();
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public CompletableFuture<Message<?>> collectAsync() {
        final CompletableFuture<Message<?>> future = new CompletableFuture<>();

        executionQueue.execute(() -> {
            final Message<?> message = messages.poll();

            if (message != null) {
                future.complete(message);
            } else {
                futures.add(future);
            }
        });

        return future;
    }

    public boolean isEmpty() {
        return messages.isEmpty();
    }
}
