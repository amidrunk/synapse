package io.synapse.exchange;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageExchange;
import org.apache.commons.lang3.Validate;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;

/**
 *
 */
public class Aggregator implements MessageExchange {

    private final Predicate<Message<?>> completionPredicate;

    private final AtomicReference<LinkedList<Object>> listRef = new AtomicReference<>(new LinkedList<>());

    public Aggregator(Predicate<Message<?>> completionPredicate) {
        this.completionPredicate = Validate.notNull(completionPredicate, "completionPredicate");
    }

    @Override
    public void processMessage(MessageContext context, Message<?> message) {
        if (completionPredicate.test(message)){
            final LinkedList<Object> objects = listRef.getAndSet(new LinkedList<>());

            context.send(Message.of(Batch.of(objects)));
        } else {
            listRef.get().add(message.getPayload());
        }
    }

    public static Aggregator on(Predicate<Message<?>> predicate) {
        return new Aggregator(predicate);
    }
}
