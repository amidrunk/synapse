package io.synapse.exchange;

import org.apache.commons.lang3.Validate;

import java.util.List;

public class Batch<T> {

    private final List<T> elements;

    private Batch(List<T> elements) {
        this.elements = Validate.notNull(elements, "elements");
    }

    public T get(int index) {
        return elements.get(index);
    }

    public int size() {
        return elements.size();
    }

    public List<T> elements() {
        return elements;
    }

    public static<T> Batch<T> of(List<T> objects) {
        Validate.notNull(objects, "objects");

        return new Batch<T>(objects);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Batch<?> batch = (Batch<?>) o;

        return elements.equals(batch.elements);

    }

    @Override
    public int hashCode() {
        return elements.hashCode();
    }

    @Override
    public String toString() {
        return "Batch{" +
                "elements=" + elements +
                '}';
    }
}
