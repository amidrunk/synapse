package io.synapse;

public class Notifications {

    public static Notification notUnderstood(Message<?> message) {
        return new NotUnderstoodNotification(message);
    }

    public static Notification undeliverable(Message<?> message) {
        return new MessageNotDeliverableNotification(message);
    }

    public static Notification messageDispatchError(MessageDispatcher source, Throwable error) {
        return new MessageDispatchErrorNotification(source, error);
    }

    public static Notification messageProcessingError(MessageExchange source, Throwable error) {
        return new MessageProcessingErrorNotification(source, error);
    }
}
