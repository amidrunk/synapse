package io.synapse;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.Validate;

import java.util.Map;
import java.util.Optional;

public class Header {

    public static final Header EMPTY = new Header(ImmutableMap.of());

    private final ImmutableMap<String, Object> attributes;

    private Header(ImmutableMap<String, Object> attributes) {
        this.attributes = ImmutableMap.copyOf(Validate.notNull(attributes, "attributes"));
    }

    public Header withAttribute(String attributeName, Object attributeValue) {
        Validate.notEmpty(attributeName, "attributeName");

        return copyWithoutAttribute(attributeName).with(attributeName, attributeValue).build();
    }

    public Header withoutAttribute(String attributeName) {
        Validate.notEmpty(attributeName, "attributeName");
        return copyWithoutAttribute(attributeName).build();
    }

    public Optional<Object> getAttribute(String attributeName) {
        Validate.notEmpty(attributeName, "attributeName");
        return Optional.ofNullable(attributes.get(attributeName));
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Header empty() {
        return EMPTY;
    }

    public static Header of(String attributeName, Object attributeValue) {
        return Header.builder().with(attributeName, attributeValue).build();
    }

    public static Header of(String attributeName1, Object attributeValue1,
                            String attributeName2, Object attributeValue2) {
        return Header.builder()
                .with(attributeName1, attributeValue1)
                .with(attributeName2, attributeValue2)
                .build();
    }

    public static class Builder {

        private final ImmutableMap.Builder<String, Object> attributes = ImmutableMap.builder();

        public Builder with(String attributeName, Object attributeValue) {
            Validate.notEmpty(attributeName, "attributeName");
            Validate.notNull(attributeValue, "attributeValue");

            attributes.put(attributeName, attributeValue);
            return this;
        }

        public Header build() {
            return new Header(attributes.build());
        }
    }

    private Builder copyWithoutAttribute(String attributeName) {
        final Builder builder = new Builder();

        attributes.entrySet().stream()
                .filter(e -> !e.getKey().equals(attributeName))
                .forEach(e -> builder.with(e.getKey(), e.getValue()));
        return builder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Header header = (Header) o;

        return attributes.equals(header.attributes);

    }

    @Override
    public int hashCode() {
        return attributes.hashCode();
    }

    @Override
    public String toString() {
        return "Header{" +
                "attributes=" + attributes +
                '}';
    }
}
