package io.synapse;

import org.apache.commons.lang3.Validate;

public class MessageProcessingErrorNotification implements MessagingErrorNotification<MessageExchange> {

    private final MessageExchange source;

    private final Throwable throwable;

    public MessageProcessingErrorNotification(MessageExchange source, Throwable throwable) {
        this.source = Validate.notNull(source, "source");
        this.throwable = Validate.notNull(throwable, "throwable");
    }

    @Override
    public MessageExchange getSource() {
        return source;
    }

    @Override
    public Throwable getThrowable() {
        return throwable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageProcessingErrorNotification that = (MessageProcessingErrorNotification) o;

        if (!source.equals(that.source)) return false;
        return throwable.equals(that.throwable);

    }

    @Override
    public int hashCode() {
        int result = source.hashCode();
        result = 31 * result + throwable.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "MessageProcessingErrorNotification{" +
                "source=" + source +
                ", throwable=" + throwable +
                '}';
    }
}
