package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageDispatcher;
import org.apache.commons.lang3.Validate;

import java.util.List;

public class FanOutMessageDispatcher implements MessageDispatcher {

    private final List<MessageDispatcher> messageDispatchers;

    public FanOutMessageDispatcher(List<MessageDispatcher> messageDispatchers) {
        this.messageDispatchers = Validate.notNull(messageDispatchers, "messageDispatchers");
    }

    @Override
    public void dispatch(MessageContext messageContext, Message<?> message) {
        for (final MessageDispatcher messageDispatcher : messageDispatchers) {
            messageDispatcher.dispatch(messageContext, message);
        }
    }

    @Override
    public void close() {
        messageDispatchers.forEach(MessageDispatcher::close);
    }
}
