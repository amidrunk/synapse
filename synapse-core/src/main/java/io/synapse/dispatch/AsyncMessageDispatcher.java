package io.synapse.dispatch;

import io.synapse.*;
import org.apache.commons.lang3.Validate;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class AsyncMessageDispatcher implements MessageDispatcher, AutoCloseable {

    private static final Object POISON = new Object();

    public enum State {
        RUNNING,
        STOPPING,
        STOPPED
    }

    private final MessageExchange destinationMessageExchange;

    private final LinkedBlockingQueue<Message<?>> queue = new LinkedBlockingQueue<>();

    private volatile State state = State.RUNNING;

    private final ReadWriteLock stateLock = new ReentrantReadWriteLock();

    private AsyncMessageDispatcher(MessageExchange destinationMessageExchange) {
        this.destinationMessageExchange = Validate.notNull(destinationMessageExchange, "destinationMessageExchange");
    }

    public void dispatch(MessageContext messageContext, Message<?> message) {
        Validate.notNull(message, "message");

        stateLock.readLock().lock();

        try {
            if (state != State.RUNNING) {
                throw new IllegalStateException("state=" + state);
            }

            queue.offer(Message.of((Runnable) () -> {
                try {
                    destinationMessageExchange.processMessage(messageContext, message);
                } catch (Throwable e) {
                    messageContext.notify(Notifications.messageProcessingError(destinationMessageExchange, e));
                }
            }));
        } finally {
            stateLock.readLock().unlock();
        }
    }

    public void close() {
        stateLock.writeLock().lock();

        try {
            if (state == State.RUNNING) {
                state = State.STOPPING;

                queue.offer(Message.of(POISON));
            }
        } finally {
            stateLock.writeLock().unlock();
        }
    }

    private void doDispatch() {
        for (;;) {
            final Message<?> message;

            try {
                message = queue.take();
            } catch (InterruptedException e) {
                Thread.interrupted();
                continue;
            }

            if (message.getPayload() == POISON) {
                break;
            }

            ((Runnable) message.getPayload()).run();
        }
    }

    public static AsyncMessageDispatcher start(MessageExchange destinationMessageExchange) {
        return start(destinationMessageExchange, Thread::new);
    }

    public static AsyncMessageDispatcher start(MessageExchange destinationMessageExchange, ThreadFactory threadFactory) {
        Validate.notNull(threadFactory, "threadFactory");

        final AsyncMessageDispatcher messageDispatcher = new AsyncMessageDispatcher(destinationMessageExchange);

        threadFactory.newThread(messageDispatcher::doDispatch).start();

        return messageDispatcher;
    }
}
