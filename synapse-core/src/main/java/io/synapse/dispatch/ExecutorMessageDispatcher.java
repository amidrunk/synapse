package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageDispatcher;
import io.synapse.MessageExchange;
import org.apache.commons.lang3.Validate;

import java.util.concurrent.Executor;

public class ExecutorMessageDispatcher implements MessageDispatcher {

    private final Executor executor;

    private final MessageExchange targetMessageExchange;

    public ExecutorMessageDispatcher(Executor executor, MessageExchange targetMessageExchange) {
        this.executor = Validate.notNull(executor, "executor");
        this.targetMessageExchange = Validate.notNull(targetMessageExchange, "targetMessageExchange");
    }

    @Override
    public void dispatch(MessageContext messageContext, Message<?> message) {
        executor.execute(() -> targetMessageExchange.processMessage(messageContext, message));
    }

    @Override
    public void close() {

    }
}
