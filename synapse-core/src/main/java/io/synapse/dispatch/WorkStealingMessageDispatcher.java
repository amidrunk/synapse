package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageDispatcher;
import io.synapse.MessageExchange;
import io.synapse.concurrent.ExecutionQueue;
import io.synapse.concurrent.WorkStealingExecutionQueue;
import org.apache.commons.lang3.Validate;

public class WorkStealingMessageDispatcher implements MessageDispatcher {

    private final ExecutionQueue executionQueue = new WorkStealingExecutionQueue();

    private final MessageExchange messageExchange;

    public WorkStealingMessageDispatcher(MessageExchange messageExchange) {
        this.messageExchange = Validate.notNull(messageExchange, "messageExchange");
    }

    @Override
    public void dispatch(MessageContext messageContext, Message<?> message) {
        executionQueue.execute(() -> messageExchange.processMessage(messageContext, message));
    }

    @Override
    public void close() {

    }
}
