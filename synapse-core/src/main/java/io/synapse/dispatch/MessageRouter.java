package io.synapse.dispatch;

import com.google.common.collect.ImmutableList;
import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageDispatcher;
import io.synapse.Notifications;
import org.apache.commons.lang3.Validate;

import java.util.function.Predicate;

public class MessageRouter implements MessageDispatcher {

    private final ImmutableList<Route> routes;

    private MessageRouter(ImmutableList<Route> routes) {
        this.routes = Validate.notNull(routes, "routes");
    }

    @Override
    public void dispatch(MessageContext messageContext, Message<?> message) {
        for (final Route route : routes) {
            if (route.getPredicate().test(message)) {
                route.getMessageDispatcher().dispatch(messageContext, message);
                return;
            }
        }

        messageContext.notify(Notifications.undeliverable(message));
    }

    @Override
    public void close() {
        routes.forEach(route -> route.getMessageDispatcher().close());
    }

    public static class Builder {

        private final ImmutableList.Builder<Route> routes = ImmutableList.builder();

        public Builder addRoute(Predicate<Message<?>> predicate, MessageDispatcher messageDispatcher) {
            Validate.notNull(predicate, "predicate");
            Validate.notNull(messageDispatcher, "messageDispatcher");

            routes.add(new Route(predicate, messageDispatcher));

            return this;
        }

        public MessageRouter build() {
            return new MessageRouter(routes.build());
        }
    }

    private static class Route {

        private final Predicate<Message<?>> predicate;

        private final MessageDispatcher messageDispatcher;

        private Route(Predicate<Message<?>> predicate, MessageDispatcher messageDispatcher) {
            this.predicate = predicate;
            this.messageDispatcher = messageDispatcher;
        }

        public Predicate<Message<?>> getPredicate() {
            return predicate;
        }

        public MessageDispatcher getMessageDispatcher() {
            return messageDispatcher;
        }
    }
}
