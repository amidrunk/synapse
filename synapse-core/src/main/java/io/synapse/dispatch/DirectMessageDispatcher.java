package io.synapse.dispatch;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageDispatcher;
import io.synapse.MessageExchange;
import org.apache.commons.lang3.Validate;

public class DirectMessageDispatcher implements MessageDispatcher {

    private final MessageExchange messageExchange;

    public DirectMessageDispatcher(MessageExchange messageExchange) {
        this.messageExchange = Validate.notNull(messageExchange, "messageExchange");
    }

    @Override
    public void dispatch(MessageContext messageContext, Message<?> message) {
        messageExchange.processMessage(messageContext, message);
    }

    @Override
    public void close() {
    }
}
