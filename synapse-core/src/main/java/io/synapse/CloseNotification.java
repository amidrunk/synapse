package io.synapse;

import org.apache.commons.lang3.Validate;

public class CloseNotification implements Notification {

    private final MessagingSystem messagingSystem;

    public CloseNotification(MessagingSystem messagingSystem) {
        this.messagingSystem = Validate.notNull(messagingSystem, "messagingSystem");
    }

    public MessagingSystem getMessagingSystem() {
        return messagingSystem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CloseNotification that = (CloseNotification) o;

        return messagingSystem.equals(that.messagingSystem);

    }

    @Override
    public int hashCode() {
        return messagingSystem.hashCode();
    }

    @Override
    public String toString() {
        return "CloseNotification{" +
                "messagingSystem=" + messagingSystem +
                '}';
    }
}
