package io.synapse;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.UnmodifiableIterator;
import io.synapse.dispatch.*;
import org.apache.commons.lang3.Validate;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MessagingSystem implements AutoCloseable {

    private final ImmutableList<Supervisor> supervisors;

    private final ImmutableList<MessageDispatcher> messageDispatchers;

    private MessagingSystem(ImmutableList<Supervisor> supervisors, ImmutableList<MessageDispatcher> messageDispatchers) {
        this.supervisors = Validate.notNull(supervisors, "supervisors");
        this.messageDispatchers = Validate.notEmpty(messageDispatchers, "messageDispatchers");
    }

    public static MessagingSystemBuilder builder() {
        return new MessagingSystemBuilder();
    }

    @Override
    public void close() {
        messageDispatchers.forEach(MessageDispatcher::close);
    }

    public MessageChannel newMessageChannel() {
        final Supervisor supervisor = notification -> supervisors.forEach(s -> s.onSupervisoryNotification(notification));

        return message -> {
            final UnmodifiableIterator<MessageDispatcher> iterator = messageDispatchers.iterator();

            MessageDispatcher first = iterator.next();

            final Message messageToDispatch;

            if (message.getConversationId().isPresent()) {
                messageToDispatch = message;
            } else {
                messageToDispatch = message.withConversationId(UUID.randomUUID().toString());
            }

            first.dispatch(newMessageContext(iterator, supervisor), messageToDispatch);
        };
    }

    private MessageContext newMessageContext(Iterator<MessageDispatcher> iterator, Supervisor supervisor) {
        if (!iterator.hasNext()) {
            return new MessageContext() {
                @Override
                public void send(Message<?> message) {
                    supervisor.onSupervisoryNotification(Notifications.undeliverable(message));
                }

                @Override
                public void notify(Notification notification) {
                    supervisor.onSupervisoryNotification(notification);
                }
            };
        }

        final MessageDispatcher messageDispatcher = iterator.next();
        final MessageContext nextMessageContext = newMessageContext(iterator, supervisor);

        return new MessageContext() {
            @Override
            public void send(Message<?> message) {
                messageDispatcher.dispatch(nextMessageContext, message);
            }

            @Override
            public void notify(Notification notification) {
                supervisor.onSupervisoryNotification(notification);
            }
        };
    }

    public static class MessagingSystemBuilder extends RoutableDispatchBuilder<MessagingSystemBuilder> {

        private final ImmutableList.Builder<Supervisor> supervisors = ImmutableList.builder();

        private final ImmutableList.Builder<MessageDispatcher> messageDispatchers = ImmutableList.builder();

        public MessagingSystemBuilder supervisedBy(Supervisor supervisor) {
            Validate.notNull(supervisor, "supervisor");
            supervisors.add(supervisor);
            return this;
        }

        @Override
        protected MessagingSystemBuilder outer() {
            return this;
        }

        @Override
        protected void register(MessageDispatcher messageDispatcher) {
            messageDispatchers.add(messageDispatcher);
        }

        public MessagingSystem build() {
            return new MessagingSystem(supervisors.build(), messageDispatchers.build());
        }
    }

    public static abstract class DispatchBuilder<Outer> {

        public Outer to(List<MessageChannel> messageChannels) {
            Validate.notNull(messageChannels, "messageChannels");

            messageChannels.forEach(messageChannel
                    -> register(new DirectMessageDispatcher((ctx, m) -> messageChannel.send(m))));

            return outer();
        }

        public Outer to(MessageChannel messageChannel) {
            Validate.notNull(messageChannel, "messageChannel");
            register(new DirectMessageDispatcher((ctx, m) -> messageChannel.send(m)));
            return outer();
        }

        public ViaExecutorContinuation<Outer> on(Executor executor) {
            return messageExchange -> {
                register(new ExecutorMessageDispatcher(executor, messageExchange));
                return outer();
            };
        }

        public interface ViaExecutorContinuation<T> {
            T to(MessageExchange messageExchange);
        }

        public Outer serializedTo(MessageExchange messageExchange) {
            Validate.notNull(messageExchange, "messageExchange");
            register(new WorkStealingMessageDispatcher(messageExchange));
            return outer();
        }

        public Outer directlyTo(MessageExchange messageExchange) {
            Validate.notNull(messageExchange, "messageExchange");
            register(new DirectMessageDispatcher(messageExchange));
            return outer();
        }

        public Outer asyncTo(MessageExchange messageExchange) {
            Validate.notNull(messageExchange, "messageExchange");
            register(AsyncMessageDispatcher.start(messageExchange));
            return outer();
        }

        protected abstract Outer outer();

        protected abstract void register(MessageDispatcher messageDispatcher);

    }

    public static abstract class RoutableDispatchBuilder<Outer> extends DispatchBuilder<Outer> {

        public RouterBuilder<Outer> route() {
            return new RouterBuilder<Outer>(this::register, outer());
        }

        public FanoutBuilder<Outer> fanout() {
            return new FanoutBuilder<Outer>(this::register, outer());
        }

    }

    public static class RouterBuilder<Outer> {

        private final Consumer<MessageDispatcher> messageDispatcherConsumer;

        private final Outer outer;

        private final MessageRouter.Builder routerBuilder = new MessageRouter.Builder();

        private RouterBuilder(Consumer<MessageDispatcher> messageDispatcherConsumer, Outer outer) {
            this.messageDispatcherConsumer = messageDispatcherConsumer;
            this.outer = outer;
        }

        public DispatchBuilder<RouterBuilder<Outer>> when(Predicate<Message<?>> predicate) {
            final RouterBuilder<Outer> outerBuilder = this;

            return new DispatchBuilder<RouterBuilder<Outer>>() {
                @Override
                protected RouterBuilder<Outer> outer() {
                    return outerBuilder;
                }

                @Override
                protected void register(MessageDispatcher messageDispatcher) {
                    routerBuilder.addRoute(predicate, messageDispatcher);
                }
            };
        }

        public Outer end() {
            messageDispatcherConsumer.accept(routerBuilder.build());
            return outer;
        }
    }

    public static class FanoutBuilder<Outer> extends DispatchBuilder<FanoutBuilder<Outer>> {

        private final Consumer<MessageDispatcher> messageDispatcherConsumer;

        private final Outer outer;

        private final ImmutableList.Builder<MessageDispatcher> messageDispatchers = ImmutableList.builder();

        private FanoutBuilder(Consumer<MessageDispatcher> messageDispatcherConsumer, Outer outer) {
            this.messageDispatcherConsumer = messageDispatcherConsumer;
            this.outer = outer;
        }

        @Override
        protected void register(MessageDispatcher messageDispatcher) {
            messageDispatchers.add(messageDispatcher);
        }

        public Outer end() {
            messageDispatcherConsumer.accept(new FanOutMessageDispatcher(messageDispatchers.build()));
            return outer;
        }

        @Override
        protected FanoutBuilder<Outer> outer() {
            return this;
        }
    }
}
