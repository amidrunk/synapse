package io.synapse;

import org.apache.commons.lang3.Validate;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Message<T> {

    private final String conversationId;

    private final T payload;

    private final Header header;

    private Message(String conversationId, T payload, Header header) {
        this.conversationId = conversationId;
        this.payload = payload;
        this.header = header;
    }

    public Optional<String> getConversationId() {
        return Optional.ofNullable(conversationId);
    }

    public Message<T> withConversationId(String conversationId) {
        Validate.notEmpty(conversationId, "conversationId");

        return new Message<T>(conversationId, payload, header);
    }

    public T getPayload() {
        return payload;
    }

    @SuppressWarnings("unchecked")
    public<S> Optional<S> getPayload(Class<S> type) {
        Validate.notNull(type, "type");

        if (payload == null || type.isInstance(payload)) {
            return Optional.of((S) payload);
        }

        return Optional.empty();
    }

    @SuppressWarnings("unchecked")
    public<S> Optional<Message<S>> as(Class<S> type) {
        Validate.notNull(type, "type");

        if (payload == null || type.isInstance(payload)) {
            return Optional.of((Message) this);
        }

        return Optional.empty();
    }

    public Header getHeader() {
        return header;
    }

    /**
     * Creates a new message as a continuation of the existing message. All message headers
     * will be inherited from this message allowing e.g. correlation headers and other meta-data
     * to be carried through a conversation.
     *
     * @param payload The payload of the new message.
     * @param <S> The type of the payload.
     * @return A new message with the same context.
     */
    public<S> Message<S> continuation(S payload) {
        return new Message<S>(conversationId, payload, header);
    }

    public Message<T> withHeader(Header header) {
        return Message.of(payload, header);
    }

    public static<T> Message<T> of(T payload) {
        return of(payload, Header.EMPTY);
    }

    public static<T> Message<T> of(T payload, Header header) {
        Validate.notNull(header, "header");
        return new Message<T>(null, payload, header);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message<?> message = (Message<?>) o;

        if (conversationId != null ? !conversationId.equals(message.conversationId) : message.conversationId != null)
            return false;
        if (payload != null ? !payload.equals(message.payload) : message.payload != null) return false;
        return header.equals(message.header);

    }

    @Override
    public int hashCode() {
        int result = conversationId != null ? conversationId.hashCode() : 0;
        result = 31 * result + (payload != null ? payload.hashCode() : 0);
        result = 31 * result + header.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "conversationId='" + conversationId + '\'' +
                ", payload=" + payload +
                ", header=" + header +
                '}';
    }
}
