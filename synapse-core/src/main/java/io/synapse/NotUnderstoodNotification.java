package io.synapse;

import org.apache.commons.lang3.Validate;

public class NotUnderstoodNotification implements Notification {

    private final Message<?> message;

    public NotUnderstoodNotification(Message<?> message) {
        this.message = Validate.notNull(message, "message");
    }

    public Message<?> getMessage() {
        return message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotUnderstoodNotification that = (NotUnderstoodNotification) o;

        return message.equals(that.message);

    }

    @Override
    public int hashCode() {
        return message.hashCode();
    }

    @Override
    public String toString() {
        return "NotUnderstoodNotification{" +
                "message=" + message +
                '}';
    }
}
