package io.synapse;

/**
 * A <code>MessageDispatcher</code> is responsible for transmitting a message
 * from a source connect one or more destination.
 */
public interface MessageDispatcher extends AutoCloseable {

    void dispatch(MessageContext messageContext, Message<?> message);

    @Override
    void close();
}
