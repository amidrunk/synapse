package io.synapse;

public interface Supervisor {

    void onSupervisoryNotification(Notification notification);

}
