package io.synapse;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MessageMatchers {

    public static MatchBuilder match() {
        return new MatchBuilder(ImmutableList.of());
    }

    public static class SimpleMessageMatcher implements MessageMatcher {

        private final List<MatchBuilder.Route> routes;

        public SimpleMessageMatcher(List<MatchBuilder.Route> routes) {
            this.routes = Validate.notNull(routes, "routes");
        }

        @Override
        public void match(MessageContext messageContext, Message<?> message) {
            Validate.notNull(message, "message");

            for (final MatchBuilder.Route route : routes) {
                if (route.getPredicate().test(message)) {
                    route.getConsumer().accept(messageContext, message);
                    break;
                }
            }
        }

        public List<MatchBuilder.Route> getRoutes() {
            return routes;
        }
    }

    public static class MatchBuilder extends SimpleMessageMatcher {

        public MatchBuilder(List<Route> routes) {
            super(routes);
        }

        @SuppressWarnings("unchecked")
        public <S> WhenContinuation<S> when(Predicate<Message<S>> predicate) {
            return consumer -> new MatchBuilder(ImmutableList.<Route>builder()
                    .addAll(getRoutes())
                    .add(new Route((Predicate) predicate, (BiConsumer) consumer))
                    .build());
        }

        public MessageMatcher otherwise(BiConsumer<MessageContext, Message<?>> consumer) {
            return new SimpleMessageMatcher(ImmutableList.<Route>builder()
                    .addAll(getRoutes())
                    .add(new Route(m -> true, consumer))
                    .build());
        }

        private static class Route {

            private final Predicate<Message<?>> predicate;

            private final BiConsumer<MessageContext, Message<?>> consumer;

            private Route(Predicate<Message<?>> predicate, BiConsumer<MessageContext, Message<?>> consumer) {
                this.predicate = predicate;
                this.consumer = consumer;
            }

            public Predicate<Message<?>> getPredicate() {
                return predicate;
            }

            public BiConsumer<MessageContext, Message<?>> getConsumer() {
                return consumer;
            }
        }
    }

    public interface WhenContinuation<S> {

        MatchBuilder then(BiConsumer<MessageContext, Message<S>> consumer);

    }

    public static <S> Predicate<Message<S>> hasPayload(Class<S> type) {
        return m -> m.getPayload() != null && type.isInstance(m.getPayload());
    }

}
