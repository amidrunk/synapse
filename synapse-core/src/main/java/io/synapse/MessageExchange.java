package io.synapse;

public interface MessageExchange {

    void processMessage(MessageContext context, Message<?> message);

}
