package io.synapse.producer;

public class Tick {

    private final long lastTickNanoTime;

    private final long nanoTime;

    public Tick(long lastTickNanoTime, long nanoTime) {
        this.lastTickNanoTime = lastTickNanoTime;
        this.nanoTime = nanoTime;
    }

    public long lastTickNanoTime() {
        return lastTickNanoTime;
    }

    public long nanoTime() {
        return nanoTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tick tick = (Tick) o;

        if (lastTickNanoTime != tick.lastTickNanoTime) return false;
        return nanoTime == tick.nanoTime;

    }

    @Override
    public int hashCode() {
        int result = (int) (lastTickNanoTime ^ (lastTickNanoTime >>> 32));
        result = 31 * result + (int) (nanoTime ^ (nanoTime >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Tick{" +
                "lastTickNanoTime=" + lastTickNanoTime +
                ", nanoTime=" + nanoTime +
                '}';
    }
}
