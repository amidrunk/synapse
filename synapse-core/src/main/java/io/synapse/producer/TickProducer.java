package io.synapse.producer;

import com.google.common.util.concurrent.AbstractService;
import io.synapse.Message;
import io.synapse.MessageChannel;
import org.apache.commons.lang3.Validate;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TickProducer extends AbstractService {

    private final ScheduledExecutorService scheduler;

    private final TickSource tickSource;

    private final MessageChannel destination;

    private volatile ScheduledFuture<?> scheduledFuture;

    private volatile long lastTick = -1L;

    private TickProducer(ScheduledExecutorService scheduler, TickSource tickSource, MessageChannel destination) {
        this.scheduler = Validate.notNull(scheduler, "scheduler");
        this.tickSource = Validate.notNull(tickSource, "tickSource");
        this.destination = Validate.notNull(destination, "destination");
    }

    @Override
    protected void doStart() {
        scheduleTick();
    }

    @Override
    protected void doStop() {
        if (this.scheduledFuture != null) {
            this.scheduledFuture.cancel(false);
        }

        this.scheduledFuture = null;
    }

    private void scheduleTick() {
        this.scheduledFuture = scheduler.schedule(() -> {
            final long nanoTime = System.nanoTime();
            destination.send(Message.of(new Tick(lastTick, nanoTime)));
            lastTick = nanoTime;
            scheduleTick();
        }, tickSource.nextTick(), TimeUnit.NANOSECONDS);
    }

    public static class Builder {

        private ScheduledExecutorService scheduler;

        private TickSource tickSource;

        private MessageChannel destination;

        public Builder setScheduler(ScheduledExecutorService scheduler) {
            this.scheduler = scheduler;
            return this;
        }

        public Builder setTickSource(TickSource tickSource) {
            this.tickSource = tickSource;
            return this;
        }

        public Builder setDestination(MessageChannel destination) {
            this.destination = destination;
            return this;
        }

        public TickProducer build() {
            return new TickProducer(scheduler, tickSource, destination);
        }
    }
}
