package io.synapse.producer;

import java.util.concurrent.TimeUnit;

public interface TickSource {

    static TickSource fixed(long duration, TimeUnit timeUnit) {
        final long tickSizeNanos = timeUnit.toNanos(duration);

        return () -> tickSizeNanos;
    }

    long nextTick();

}
