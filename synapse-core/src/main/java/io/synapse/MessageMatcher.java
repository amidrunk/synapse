package io.synapse;

public interface MessageMatcher {

    void match(MessageContext messageContext, Message<?> message);

}
