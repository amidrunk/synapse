package io.synapse.data;

import org.apache.commons.lang3.Validate;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletionStage;

public class SetMemberFilter<T> implements MemberFilter<T> {

    private final Set<T> set = new HashSet<T>(); // Can be provided

    private final HashFunction<T> hashFunction;

    public SetMemberFilter(HashFunction<T> hashFunction) {
        this.hashFunction = Validate.notNull(hashFunction, "hashFunction");
    }

    @Override
    public CompletionStage<MemberStatus> testAndAdd(T object) {
        Validate.notNull(object, "object");
        return null;
    }

    @Override
    public CompletionStage<MemberStatus> test(T object) {
        return null;
    }

    @Override
    public CompletionStage<Void> add(T object) {
        return null;
    }

    private static class Entry<T> {

        private final T object;

        private final HashFunction<T> hashFunction;

        public Entry(T object, HashFunction<T> hashFunction) {
            this.object = Validate.notNull(object, "object");
            this.hashFunction = Validate.notNull(hashFunction, "hashFunction");
        }

        @Override
        public int hashCode() {
            return hashFunction.hash(object);
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
    }
}
