package io.synapse.data;

import org.apache.commons.lang3.Validate;

import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

public class FileMemberFilter<T> implements MemberFilter<T> {

    private final Path indexPath;

    private final HashFunction<T> hashFunction;

    private final SerializationFunction<T> serializationFunction;

    public FileMemberFilter(Path indexPath, int blockCount, HashFunction<T> hashFunction, SerializationFunction<T> serializationFunction) {
        this.indexPath = Validate.notNull(indexPath, "indexPath");
        this.hashFunction = Validate.notNull(hashFunction, "hashFunction");
        this.serializationFunction = Validate.notNull(serializationFunction, "serializationFunction");
    }

    @Override
    public CompletableFuture<MemberStatus> testAndAdd(T object) {
        return null;
    }

    @Override
    public CompletableFuture<MemberStatus> test(T object) {
        return null;
    }

    @Override
    public CompletableFuture<Void> add(T object) {
        return null;
    }

    public static class Block {



    }
}
