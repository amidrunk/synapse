package io.synapse.data;

public interface HashFunction<T> {

    int hash(T instance);

    static<T> HashFunction<T> defaultHashCode() {
        return Object::hashCode;
    }
}
