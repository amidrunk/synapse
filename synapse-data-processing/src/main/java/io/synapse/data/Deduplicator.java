package io.synapse.data;

import io.synapse.Message;
import io.synapse.MessageContext;
import io.synapse.MessageExchange;
import io.synapse.Notifications;
import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.function.Predicate;
import java.util.logging.Logger;

// This can be done smarter... do pull composition instead. As long as there's an ordered source of data...
public class Deduplicator implements MessageExchange {

    private static final Logger LOGGER = Logger.getLogger(Deduplicator.class.getName());

    private static final int BLOCK_COUNT = 256;

    private static final int HEADER_LENGTH = 2;

    private static final int FLUSH_BUFFER_SIZE = 16384;

    private static final int READ_BUFFER_SIZE = 16384;

    private final MessageMarshaller messageMarshaller;

    private final Predicate<Message<?>> terminationPredicate;

    private final LinkedList<Path> dataFiles = new LinkedList<>();

    private final Path path;

    private final long blockSize;

    private Block[] blocks;

    private long bytesWritten = 0;

    private long messageCount = 0;

    private Deduplicator(MessageMarshaller messageMarshaller, Predicate<Message<?>> terminationPredicate, Path path, long blockSize) {
        Validate.isTrue(blockSize > 0, "blockSize must be greater than zero");

        this.messageMarshaller = Validate.notNull(messageMarshaller, "messageMarshaller");
        this.terminationPredicate = Validate.notNull(terminationPredicate, "terminationPredicate");
        this.path = Validate.notNull(path, "path");
        this.blockSize = blockSize;

        resetBlocks();
    }

    @Override
    public void processMessage(MessageContext context, Message<?> message) {
        if (terminationPredicate.test(message)) {
            LOGGER.info("Termination message received, starting stream of deduplicated values");

            final ArrayList<BufferSource> sources = new ArrayList<>(dataFiles.size() + 1);

            for (final Path path : dataFiles) {
                try {
                    sources.add(new FileBufferSource(path));
                } catch (IOException e) {
                    context.notify(Notifications.messageProcessingError(this, e));
                    return;
                }
            }

            sources.add(new InMemoryBufferSource(Arrays.asList(blocks).iterator()));

            for (final Iterator<BufferSource> i = sources.iterator(); i.hasNext(); ) {
                try {
                    if (!i.next().next()) {
                        i.remove();
                    }
                } catch (IOException e) {
                    context.notify(Notifications.messageProcessingError(this, e));
                    return;
                }
            }

            while (!sources.isEmpty()) {
                BufferSource sourceOfSmallestValue = null;

                for (final Iterator<BufferSource> i = sources.iterator(); i.hasNext(); ) {
                    final BufferSource currentSource = i.next();

                    if (sourceOfSmallestValue == null) {
                        sourceOfSmallestValue = currentSource;
                    } else {
                        final int comparison = compare(currentSource.get(), sourceOfSmallestValue.get());

                        if (comparison == 0) {
                            try {
                                if (!currentSource.next()) {
                                    i.remove();
                                }
                            } catch (IOException e) {
                                context.notify(Notifications.messageProcessingError(this, e));
                                return;
                            }
                        }

                        if (comparison < 0) {
                            sourceOfSmallestValue = currentSource;
                        }
                    }
                }

                if (sourceOfSmallestValue != null) {
                    final byte[] result = sourceOfSmallestValue.get();

                    try {
                        if (!sourceOfSmallestValue.next()) {
                            sources.remove(sourceOfSmallestValue);
                        }
                    } catch (IOException e) {
                        context.notify(Notifications.messageProcessingError(this, e));
                        return;
                    }

                    context.send(messageMarshaller.unmarshal(result));
                }
            }

            context.send(message);
        } else {
            messageCount++;

            final byte[] buffer = messageMarshaller.marshal(message);

            long newBytesWritten = bytesWritten + buffer.length + HEADER_LENGTH;

            if (newBytesWritten > blockSize) {
                try {
                    flush();
                } catch (IOException e) {
                    context.notify(Notifications.messageProcessingError(this, e));
                    return;
                }

                newBytesWritten = buffer.length + HEADER_LENGTH;
            }

            final Block block = this.blocks[(int) buffer[0] & 0xFF];

            block.insert(buffer);

            this.bytesWritten = newBytesWritten;
        }
    }

    private void flush() throws IOException {
        final Path path = this.path.resolve(UUID.randomUUID().toString() + ".dat");

        dataFiles.add(path);

        LOGGER.info("Flushing current block to path: " + path + " (" + bytesWritten/1024 + " Kb / " + messageCount + " messages)");

        try (final FileChannel channel = FileChannel.open(path, StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW)) {
            final ByteBuffer buffer = ByteBuffer.allocate(FLUSH_BUFFER_SIZE);

            for (final Block block : blocks) {
                for (final byte[] data : block.data) {
                    final int length = data.length + HEADER_LENGTH;

                    if (buffer.remaining() < length) {
                        buffer.flip();
                        channel.write(buffer);
                        buffer.clear();
                    }

                    buffer.putShort((short) (data.length & 0xFFFF));
                    buffer.put(data);
                }
            }

            if (buffer.position() > 0) {
                buffer.flip();
                channel.write(buffer);
            }
        }

        resetBlocks();
    }

    private void resetBlocks() {
        this.blocks = new Block[BLOCK_COUNT];

        for (int i = 0; i < blocks.length; i++) {
            blocks[i] = new Block();
        }
    }

    public static WithMarshallerContinuation withMessageMarshaller(MessageMarshaller messageMarshaller) {
        return terminationPredicate -> path -> blockSizeInBytes -> new Deduplicator(messageMarshaller, terminationPredicate, path, blockSizeInBytes);
    }

    public interface WithMarshallerContinuation {

        UntilContinuation until(Predicate<Message<?>> terminationPredicate);

    }

    public interface UntilContinuation {

        WithFileAreaContinuation withFileArea(Path path);

    }

    public interface WithFileAreaContinuation {

        Deduplicator andMaximumBlockSize(long blockSizeInBytes);

    }

    public static class Block {

        private final LinkedList<byte[]> data = new LinkedList<>();

        public boolean insert(byte[] newBuffer) {
            final ListIterator<byte[]> listIterator = this.data.listIterator();

            while (listIterator.hasNext()) {
                final byte[] existingBuffer = listIterator.next();
                final int comparison = compare(newBuffer, existingBuffer);

                if (comparison == 0) {
                    return false;
                } else if (comparison < 0) {
                    listIterator.previous();
                    listIterator.add(newBuffer);
                    return true;
                }
            }

            this.data.add(newBuffer);
            return true;
        }
    }

    private static int compare(byte[] left, byte[] right){
        final int compareLength = (left.length < right.length ? left.length : right.length);

        for (int i = 0; i < compareLength; i++) {
            if (left[i] == right[i]) {
                if (i == compareLength - 1 && left.length == right.length) {
                    return 0;
                }
            } else {
                return ((int) left[i] & 0xFF) - ((int) right[i] & 0xFF);
            }
        }

        return (left.length < right.length ? -1 : 1);
    }

    public interface BufferSource {

        byte[] get();

        boolean next() throws IOException;

    }

    public static class InMemoryBufferSource implements BufferSource {

        private final Iterator<Block> blockIterator;

        private Iterator<byte[]> bufferIterator;

        private byte[] currentBuffer;

        private InMemoryBufferSource(Iterator<Block> blockIterator) {
            this.blockIterator = blockIterator;
        }

        @Override
        public byte[] get() {
            return currentBuffer;
        }

        @Override
        public boolean next() {
            if (bufferIterator == null || !bufferIterator.hasNext()) {
                while (blockIterator.hasNext()) {
                    bufferIterator = blockIterator.next().data.iterator();

                    if (bufferIterator.hasNext()) {
                        swap(bufferIterator.next());
                        return true;
                    }
                }

                return false;
            }

            swap(bufferIterator.next());

            return true;
        }

        private void swap(byte[] newBuffer) {
            this.currentBuffer = newBuffer;
        }
    }

    public static class FileBufferSource implements BufferSource {

        private final Path path;

        private final FileChannel fileChannel;

        private final ByteBuffer byteBuffer = ByteBuffer.allocate(READ_BUFFER_SIZE);

        private byte[] currentBuffer;

        private boolean eof = false;

        private FileBufferSource(Path path) throws IOException {
            this.path = path;
            this.fileChannel = FileChannel.open(path, StandardOpenOption.READ);

            read();
        }

        @Override
        public byte[] get() {
            return currentBuffer;
        }

        @Override
        public boolean next() throws IOException {
            if (byteBuffer.hasRemaining() && byteBuffer.remaining() > 2) {
                final int length = byteBuffer.getShort();

                if (byteBuffer.remaining() < length) {
                    byteBuffer.position(byteBuffer.position() - 2);
                } else {
                    final byte[] newBuffer = new byte[length];

                    byteBuffer.get(newBuffer);

                    swap(newBuffer);

                    return true;
                }
            }

            if (eof) {
                return false;
            }

            if (byteBuffer.hasRemaining()) {
                final byte[] temp = new byte[byteBuffer.remaining()];

                byteBuffer.get(temp);
                byteBuffer.clear();
                byteBuffer.put(temp);
            } else {
                byteBuffer.clear();
            }

            read();

            return next();
        }

        private void read() throws IOException {
            fileChannel.read(byteBuffer);

            if (byteBuffer.hasRemaining()) {
                eof = true;
            }

            byteBuffer.flip();
        }

        private void swap(byte[] newBuffer) {
            if (currentBuffer != null && compare(newBuffer, currentBuffer) <= 0) {
                System.out.println("What the FUCK?!");
            }
            this.currentBuffer = newBuffer;
        }
    }
}
