package io.synapse.data;

import java.util.concurrent.CompletionStage;

public interface MemberFilter<T> {

    CompletionStage<MemberStatus> testAndAdd(T object);

    CompletionStage<MemberStatus> test(T object);

    CompletionStage<Void> add(T object);
}
