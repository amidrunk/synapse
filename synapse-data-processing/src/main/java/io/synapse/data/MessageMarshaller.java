package io.synapse.data;

import io.synapse.Message;

public interface MessageMarshaller {

    byte[] marshal(Message<?> message);

    Message<?> unmarshal(byte[] buffer);

}
