package io.synapse.data;

public enum MemberStatus {
    DEFINITELY_NOT,
    DEFINITELY,
    POSSIBLY
}
