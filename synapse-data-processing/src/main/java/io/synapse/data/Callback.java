package io.synapse.data;

public interface Callback<T> {

    void onComplete(T result, Throwable error);

}
