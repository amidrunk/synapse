package io.synapse.data.dedup;

import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.NoSuchElementException;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

public class BinaryFileDataView implements DataView<byte[]> {

    private static final int HEADER_LENGTH = 2;

    private final Path path;

    private final int streamBufferSize;

    private BinaryFileDataView(Path path, int streamBufferSize) {
        Validate.isTrue(streamBufferSize > 0, "streamBufferSize must be greater than zero");

        this.path = Validate.notNull(path, "path");
        this.streamBufferSize = streamBufferSize;
    }

    @Override
    public ViewStream<byte[]> stream() {
        return FileViewStream.create(path, streamBufferSize);
    }

    public static FlushConfigurationBuilder from(DataView<byte[]> block) {
        Validate.notNull(block, "block");
        return new FlushConfigurationBuilder(block);
    }

    public static class FlushConfigurationBuilder {

        private final DataView<byte[]> block;

        private int flushBufferSize = 4096;

        private int streamBufferSize = 4096;

        private FlushConfigurationBuilder(DataView<byte[]> block) {
            this.block = block;
        }

        public FlushConfigurationBuilder withFlushBufferSize(int bufferSize) {
            Validate.isTrue(bufferSize > 0, "flushBufferSize must be greater than zero");
            this.flushBufferSize = bufferSize;
            return this;
        }

        public FlushConfigurationBuilder withStreamBufferSize(int streamBufferSize) {
            Validate.isTrue(streamBufferSize > 0, "streamBufferSize must be greater than zero");
            this.streamBufferSize = streamBufferSize;
            return this;
        }

        public BinaryFileDataView flushInto(Path path) throws IOException {
            Validate.notNull(path, "path");

            final ByteBuffer flushBuffer = ByteBuffer.allocate(flushBufferSize);

            try (final FileChannel channel = FileChannel.open(path, WRITE, CREATE)) {
                try (final ViewStream<byte[]> stream = block.stream()) {
                    while (stream.next()) {
                        final byte[] currentBuffer = stream.current();

                        if (currentBuffer.length + HEADER_LENGTH > flushBuffer.remaining()) {
                            flushBuffer.flip();
                            channel.write(flushBuffer);
                            flushBuffer.clear();
                        }

                        flushBuffer.putShort((short)(currentBuffer.length & 0xFFFF));
                        flushBuffer.put(currentBuffer);
                    }

                    flushBuffer.flip();

                    channel.write(flushBuffer);
                }
            }

            return new BinaryFileDataView(path, streamBufferSize);
        }
    }

    private static class FileViewStream implements ViewStream<byte[]> {

        private final ByteBuffer streamBuffer;
        private final FileChannel channel;
        private boolean eof;
        private byte[] currentBuffer;
        private long totalBytesRead;

        public static FileViewStream create(Path path, int bufferSize) {
            final FileChannel channel;

            try {
                channel = FileChannel.open(path, StandardOpenOption.READ);
            } catch (IOException e) {
                throw new ViewStreamException("Failed to ope path for streaming: " + path, e);
            }

            final FileViewStream stream = new FileViewStream(channel, ByteBuffer.allocate(bufferSize));

            stream.fillBufferFromFile();

            return stream;
        }

        public FileViewStream(FileChannel channel, ByteBuffer streamBuffer) {
            this.streamBuffer = streamBuffer;
            this.channel = channel;
            eof = false;
        }

        @Override
        public byte[] current() {
            if (currentBuffer == null) {
                throw new NoSuchElementException();
            }

            return currentBuffer;
        }

        @Override
        public boolean next() {
            if (!streamBuffer.hasRemaining()) {
                if (eof) {
                    return false;
                }

                streamBuffer.clear();
                fillBufferFromFile();
                return next();
            }

            if (streamBuffer.remaining() < HEADER_LENGTH) {
                rollOver();
                return next();
            }

            final int nextBufferSize = (int) streamBuffer.getShort() & 0xFFFF;

            if (streamBuffer.remaining() < nextBufferSize) {
                rollOverAndInsertHeader((short)(nextBufferSize & 0xFFFF));
                return next();
            }

            final byte[] result = new byte[nextBufferSize];

            streamBuffer.get(result);

            this.currentBuffer = result;

            return true;
        }

        private void rollOver() {
            final byte[] exchange = new byte[streamBuffer.remaining()];

            streamBuffer.get(exchange);
            streamBuffer.clear();
            streamBuffer.put(exchange);

            fillBufferFromFile();
        }

        private void rollOverAndInsertHeader(short header) {
            final byte[] exchange = new byte[streamBuffer.remaining()];

            streamBuffer.get(exchange);
            streamBuffer.clear();
            streamBuffer.putShort((short) (header & 0xFFFF));
            streamBuffer.put(exchange);

            fillBufferFromFile();
        }

        private void fillBufferFromFile() {
            try {
                totalBytesRead += channel.read(streamBuffer);
            } catch (IOException e) {
                throw new ViewStreamException("Failed to read from file channel", e);
            }

            if (streamBuffer.hasRemaining()) {
                eof = true;

                try {
                    channel.close();
                } catch (IOException e) {
                    throw new ViewStreamException("Failed to close underlying file channel", e);
                }
            }

            streamBuffer.flip();
        }
    }
}
