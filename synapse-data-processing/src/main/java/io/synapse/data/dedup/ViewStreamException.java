package io.synapse.data.dedup;

public class ViewStreamException extends RuntimeException {

    public ViewStreamException() {
    }

    public ViewStreamException(String message) {
        super(message);
    }

    public ViewStreamException(String message, Throwable cause) {
        super(message, cause);
    }

    public ViewStreamException(Throwable cause) {
        super(cause);
    }

    public ViewStreamException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
