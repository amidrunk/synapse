package io.synapse.data.dedup;

import java.io.IOException;

public interface Flusher<T> {

    DataView<T> flush(DataView<T> dataView) throws IOException;

    int approximateSizeOf(T instance);

}
