package io.synapse.data.dedup;

public interface ViewStream<T> extends AutoCloseable {

    T current();

    boolean next();

    @Override
    default void close() {
    }
}
