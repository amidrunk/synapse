package io.synapse.data.dedup;

import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BinaryDeduplicator implements Deduplicator<byte[]> {

    private static final Logger LOGGER = Logger.getLogger(BinaryDeduplicator.class.getName());

    private final CopyOnWriteArrayList<DataView<byte[]>> flushedDataStores = new CopyOnWriteArrayList<>();

    private DataStore<byte[]> currentDataStore;

    private final Supplier<DataStore<byte[]>> fastDataStoreSupplier;

    private final Flusher<byte[]> flusher;

    private final long maximumFileSize;

    private long currentBlockSize;

    private long totalBytesWritten;

    public BinaryDeduplicator(Supplier<DataStore<byte[]>> fastDataStoreSupplier,
                              Flusher<byte[]> flusher,
                              long maximumFileSize) {
        Validate.isTrue(maximumFileSize > 0, "maximumFileSize must be greater than zero");

        this.fastDataStoreSupplier = Validate.notNull(fastDataStoreSupplier, "fastDataStoreSupplier");
        this.flusher = Validate.notNull(flusher, "flusher");
        this.maximumFileSize = maximumFileSize;
        this.currentDataStore = fastDataStoreSupplier.get();
    }

    @Override
    public ViewStream<byte[]> stream() {
        final ArrayList<DataView<byte[]>> flushedDataStores = new ArrayList<>(this.flushedDataStores);
        final ArrayList<ViewStream<byte[]>> viewStreams = new ArrayList<>(flushedDataStores.size() + 1);

        viewStreams.add(currentDataStore.stream());

        flushedDataStores.forEach(ds -> viewStreams.add(ds.stream()));

        for (final Iterator<ViewStream<byte[]>> i = viewStreams.iterator(); i.hasNext(); ) {
            if (!i.next().next()) {
                i.remove();
            }
        }

        return new ViewStream<byte[]>() {

            private byte[] currentElement = null;

            @Override
            public byte[] current() {
                if (currentElement == null) {
                    throw new NoSuchElementException();
                }

                return currentElement;
            }

            @Override
            public boolean next() {
                ViewStream<byte[]> nextViewStream = null;

                for (final Iterator<ViewStream<byte[]>> i = viewStreams.iterator(); i.hasNext(); ) {
                    final ViewStream<byte[]> currentViewStream = i.next();

                    byte[] currentElement = currentViewStream.current();

                    if (nextViewStream == null) {
                        nextViewStream = currentViewStream;
                    } else {
                        int compareValue = Buffers.compare(currentElement, nextViewStream.current());

                        if (compareValue == 0) {
                            boolean removed = false;

                            do {
                                if (!currentViewStream.next()) {
                                    i.remove();
                                    removed = true;
                                } else {
                                    currentElement = currentViewStream.current();
                                    compareValue = Buffers.compare(currentElement, nextViewStream.current());
                                }
                            } while (compareValue == 0 && !removed);

                            if (removed) {
                                continue;
                            }
                        }

                        if (compareValue < 0) {
                            nextViewStream = currentViewStream;
                        }
                    }
                }

                if (nextViewStream == null) {
                    currentElement = null;
                    return false;
                }

                currentElement = nextViewStream.current();

                forwardStreamFromCurrentElement(nextViewStream);

                return true;
            }

            private void forwardStreamFromCurrentElement(ViewStream<byte[]> nextViewStream) {
                for (;;) {
                    if (!nextViewStream.next()) {
                        viewStreams.remove(nextViewStream);
                        break;
                    }

                    if (Buffers.compare(nextViewStream.current(), currentElement) != 0) {
                        break;
                    }
                }
            }
        };
    }

    @Override
    public boolean put(byte[] data) {
        final int elementSize = flusher.approximateSizeOf(data);
        final long newBlockSize = currentBlockSize + elementSize;

        if (newBlockSize > maximumFileSize) {
            final DataView<byte[]> dataView;

            try {
                dataView = flusher.flush(currentDataStore);
            } catch (IOException e) {
                throw new ViewStreamException("Failed to flush data store", e);
            }

            flushedDataStores.add(dataView);

            totalBytesWritten += currentBlockSize;

            if (LOGGER.isLoggable(Level.INFO)) {
                LOGGER.info("Flushed block of size " + currentBlockSize + " bytes (" + totalBytesWritten + " bytes flushed)");
            }

            currentDataStore = fastDataStoreSupplier.get();
            currentBlockSize = 0;
        }

        currentBlockSize += elementSize;

        return currentDataStore.put(data);
    }

    public static SortedBlocksBuilder sortedBlocks() {
        return new SortedBlocksBuilder();
    }

    public static class SortedBlocksBuilder {

        private Path dataDirectory;

        private int maximumFileSize = 1048576;

        private int flushBufferSize = 131072;

        private int streamBufferSize = 131072;

        private SortedBlocksBuilder() {
        }

        public SortedBlocksBuilder setDataDirectory(Path dataDirectory) {
            this.dataDirectory = dataDirectory;
            return this;
        }

        public SortedBlocksBuilder setMaximumFileSize(int maximumFileSize) {
            this.maximumFileSize = maximumFileSize;
            return this;
        }

        public SortedBlocksBuilder setFlushBufferSize(int flushBufferSize) {
            this.flushBufferSize = flushBufferSize;
            return this;
        }

        public SortedBlocksBuilder setStreamBufferSize(int streamBufferSize) {
            this.streamBufferSize = streamBufferSize;
            return this;
        }

        public BinaryDeduplicator build() {
            final Flusher<byte[]> flusher = new Flusher<byte[]>() {
                @Override
                public DataView<byte[]> flush(DataView<byte[]> dataView) throws IOException {
                    return BinaryFileDataView.from(dataView)
                            .withFlushBufferSize(131072)
                            .withStreamBufferSize(131072)
                            .flushInto(dataDirectory.resolve(UUID.randomUUID().toString() + ".dat"));
                }

                @Override
                public int approximateSizeOf(byte[] instance) {
                    return instance.length + 2;
                }
            };

            return new BinaryDeduplicator(SortedUniqueInMemoryBinaryDataStore::new, flusher, maximumFileSize);
        }
    }
}
