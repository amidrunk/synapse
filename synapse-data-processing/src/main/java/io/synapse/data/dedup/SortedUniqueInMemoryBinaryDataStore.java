package io.synapse.data.dedup;

import java.util.*;

/**
 * A block that maintains a large set of binary buffers in a sorted structure. The
 * block stream will return all buffers in accordance with a predefined sort order.
 */
public class SortedUniqueInMemoryBinaryDataStore implements DataStore<byte[]> {

    /**
     * Segments indexed by the first byte in the buffer.
     */
    private final Segment[] segments = new Segment[256];

    public SortedUniqueInMemoryBinaryDataStore() {
        Arrays.setAll(segments, n -> new Segment());
    }

    @Override
    public boolean put(byte[] data) {
        if (data == null || data.length == 0) {
            throw new IllegalArgumentException("data can't be null or empty");
        }

        return segments[(int) data[0] & 0xFF].put(data);
    }

    @Override
    public ViewStream<byte[]> stream() {
        return new ViewStream<byte[]>() {

            private int currentSegment = 0;

            private byte[] currentElement = null;

            private Iterator<byte[]> elementIterator = null;

            @Override
            public byte[] current() {
                if (currentElement == null) {
                    throw new NoSuchElementException();
                }

                return currentElement;
            }

            @Override
            public boolean next() {
                if (currentSegment >= segments.length) {
                    return false;
                }

                if (elementIterator != null && elementIterator.hasNext()) {
                    currentElement = elementIterator.next();
                    return true;
                }

                while (currentSegment < segments.length) {
                    elementIterator = segments[currentSegment++].elements.iterator();

                    if (elementIterator.hasNext()) {
                        currentElement = elementIterator.next();
                        return true;
                    }
                }

                return false;
            }
        };
    }

    public static class Segment {

        private final TreeSet<byte[]> elements = new TreeSet<>(Buffers::compare);

        private boolean put(byte[] buffer) {
            return elements.add(buffer);
        }
    }
}
