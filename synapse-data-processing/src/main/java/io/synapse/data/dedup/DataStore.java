package io.synapse.data.dedup;

public interface DataStore<T> extends DataView<T> {

    boolean put(T data);

}
