package io.synapse.data.dedup;

public interface DataView<T> {

    ViewStream<T> stream();

}
