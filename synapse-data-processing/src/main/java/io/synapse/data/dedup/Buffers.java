package io.synapse.data.dedup;

public class Buffers {

    public static int compare(byte[] left, byte[] right) {
        final int compareLength = (left.length < right.length ? left.length : right.length);

        for (int i = 0; i < compareLength; i++) {
            final int compareValue = ((int) left[i] & 0xFF) - ((int) right[i] & 0xFF);

            if (compareValue != 0) {
                return compareValue;
            }
        }

        return left.length - right.length;
    }
}
