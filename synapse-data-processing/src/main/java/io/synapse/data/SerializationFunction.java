package io.synapse.data;

public interface SerializationFunction<T> {

    byte[] serialize(T instance);

}
