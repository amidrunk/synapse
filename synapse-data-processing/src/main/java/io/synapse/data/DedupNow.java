package io.synapse.data;

import io.synapse.Message;
import io.synapse.MessageChannel;
import io.synapse.MessagingSystem;
import io.synapse.csv.*;
import io.synapse.data.dedup.BinaryDeduplicator;
import io.synapse.data.dedup.ViewStream;
import io.synapse.exchange.Aggregator;
import io.synapse.exchange.Mailbox;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by andnio on 13/10/16.
 */
public class DedupNow {

    public static void main(String[] args) throws Exception {
        File tempDirectory;

        tempDirectory = new File(System.getProperty("java.io.tmpdir"), "TEST-" + UUID.randomUUID().toString());
        tempDirectory.mkdirs();

        System.out.println("Storing intermediate files in " + tempDirectory.getAbsolutePath());

        final BinaryDeduplicator deduplicator = BinaryDeduplicator.sortedBlocks()
                .setDataDirectory(tempDirectory.toPath())
                .setMaximumFileSize(20 * 1048576)
                .build();

        final CompletableFuture<Integer> future = new CompletableFuture<>();
        final AtomicInteger addedEmailAddresses = new AtomicInteger();
        final AtomicInteger dedupedEmailAddresses = new AtomicInteger();

        final MessageChannel channel = MessagingSystem.builder()
                .directlyTo(CsvProducer.byDefault())
                .directlyTo((ctx, m) -> {
                    m.getPayload(CsvRow.class).ifPresent(row -> {
                        final String emailAddress = row.getCells()[10];

                        if (!StringUtils.isEmpty(emailAddress)) {
                            ctx.send(Message.of(emailAddress));
                        }
                    });
                    m.getPayload(EndOfFile.class).ifPresent(eof -> ctx.send(m));
                })
                .directlyTo((ctx, m) -> {
                    m.getPayload(String.class).ifPresent(str -> {
                        if (deduplicator.put(str.getBytes(StandardCharsets.UTF_8))) {
                            addedEmailAddresses.incrementAndGet();
                        } else {
                            dedupedEmailAddresses.incrementAndGet();
                        }
                    });
                    m.getPayload(EndOfFile.class).ifPresent(eof -> {
                        try (final ViewStream<byte[]> stream = deduplicator.stream()) {
                            int n = 0;

                            while (stream.next()) {
                                final String string = new String(stream.current(), StandardCharsets.UTF_8);
                                //ctx.send(Message.of(string));

                                if (++n % 100000 == 0) {
                                    System.out.println("Sent " + n + " messages");
                                }
                            }

                            future.complete(n);
                        }
                    });
                })
                .build()
                .newMessageChannel();

        channel.send(Message.of(ReadCsvFile.of(Paths.get("/Users/andnio/Playground/neo4j-dwh/in", "customers_20092016.csv"), CsvFormat.byDefault())));

        System.out.println("Received in total " + future.get() + " email addresses (" + addedEmailAddresses + " successfully added, " + dedupedEmailAddresses + " deduped immediately)");

        FileUtils.forceDelete(tempDirectory);
    }
}
