package io.synapse.data;

import org.apache.commons.lang3.Validate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

public class CachedFileBlock implements MemberFilter<byte[]> {

    private final Path file;

    private final AsynchronousFileChannel channel;

    private final Supplier<MemberFilter<byte[]>> cacheSupplier;

    private MemberFilter<byte[]> cache;

    private final AtomicLong offset = new AtomicLong();

    private final LinkedBlockingQueue<byte[]> pendingEntries = new LinkedBlockingQueue<>();

    private final AtomicInteger pendingWriteSize = new AtomicInteger();

    public CachedFileBlock(Path file, Supplier<MemberFilter<byte[]>> cacheSupplier) throws IOException {
        this.file = Validate.notNull(file, "file");
        this.cacheSupplier = Validate.notNull(cacheSupplier, "cacheSupplier");
        this.channel = AsynchronousFileChannel.open(file, StandardOpenOption.CREATE_NEW, StandardOpenOption.READ, StandardOpenOption.WRITE);
    }

    @Override
    public CompletionStage<MemberStatus> testAndAdd(byte[] object) {
        return null;
    }

    @Override
    public CompletionStage<MemberStatus> test(byte[] object) {
        return null;
    }

    @Override
    public CompletionStage<Void> add(byte[] object) {
        if (cache != null) {
            cache.add(object);
        }

        pendingWriteSize.addAndGet(4 + object.length);
        pendingEntries.offer(object);

        return null; // Should probably return a future completed when the entry is flushed...?
    }

    public CompletableFuture<Void> load() {
        return null;
    }

    public CompletableFuture<Void> unload() {
        return null;
    }

    public CompletionStage<Void> flush() throws IOException {
        final int totalNumberOfBytes = pendingWriteSize.get();
        int bytesToWrite = totalNumberOfBytes;

        final ByteBuffer buffer = ByteBuffer.allocate(bytesToWrite);

        while (bytesToWrite > 0) {
            final byte[] entry;

            try {
                entry = pendingEntries.take();
            } catch (InterruptedException e) {
                Thread.interrupted();
                continue;
            }

            buffer.putInt(entry.length);
            buffer.put(entry);

            bytesToWrite -= entry.length + 4;
        }

        final long newOffset = offset.getAndAdd(totalNumberOfBytes);

        final CompletableFuture<Void> future = new CompletableFuture<>();

        channel.write(buffer, newOffset, null, new CompletionHandler<Integer, Object>() {
            @Override
            public void completed(Integer result, Object attachment) {
                future.complete(null);
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                future.completeExceptionally(exc);
            }
        });

        return future;
    }

    public static class Entry {

        private final byte[] data;

        private Entry(byte[] data) {
            this.data = data;
        }

        public byte[] getData() {
            return data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Entry entry = (Entry) o;

            return Arrays.equals(data, entry.data);

        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(data);
        }
    }
}
