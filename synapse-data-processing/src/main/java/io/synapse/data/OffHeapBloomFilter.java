package io.synapse.data;

import org.apache.commons.lang3.Validate;

import java.nio.ByteBuffer;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class OffHeapBloomFilter<T> implements BloomFilter<T> {

    private final HashFunction<T> hashFunction;

    private final ByteBuffer buffer;

    private final int totalCapacity;

    public OffHeapBloomFilter(HashFunction<T> hashFunction, int size) {
        Validate.notNull(hashFunction, "hashFunction");
        Validate.isTrue(size > 0, "size must be greater than zero");

        this.hashFunction = hashFunction;
        this.buffer = ByteBuffer.allocateDirect((int) Math.ceil(size / 8d));
        this.totalCapacity = buffer.capacity() * 8;
    }

    @Override
    public CompletionStage<MemberStatus> testAndAdd(T object) {
        Validate.notNull(object, "object");

        final int hashValue = hashFunction.hash(object) % totalCapacity;
        final int bucketNumber = hashValue / 8;
        final int bucketIndex = hashValue % 8;

        final byte bucketValue = buffer.get(bucketNumber);
        final boolean set = (bucketValue & (1 << bucketIndex)) != 0;

        if (set) {
            return CompletableFuture.completedFuture(MemberStatus.POSSIBLY);
        }

        buffer.put(bucketNumber, (byte)((bucketValue | (1 << bucketIndex)) & 0xFF));

        return CompletableFuture.completedFuture(MemberStatus.DEFINITELY_NOT);
    }

    @Override
    public CompletionStage<MemberStatus> test(T object) {
        Validate.notNull(object, "object");

        final int hashValue = hashFunction.hash(object) % totalCapacity;
        final int bucketNumber = hashValue / 8;
        final int bucketIndex = hashValue % 8;
        final byte bucketValue = buffer.get(bucketNumber);
        final boolean set = (bucketValue & (1 << bucketIndex)) != 0;

        if (set) {
            return CompletableFuture.completedFuture(MemberStatus.POSSIBLY);
        }

        return CompletableFuture.completedFuture(MemberStatus.DEFINITELY_NOT);
    }

    @Override
    public CompletableFuture<Void> add(T object) {
        Validate.notNull(object, "object");

        final int hashValue = hashFunction.hash(object) % totalCapacity;
        final int bucketNumber = hashValue / 8;
        final int bucketIndex = hashValue % 8;
        final byte bucketValue = buffer.get(bucketNumber);

        buffer.put(bucketNumber, (byte)((bucketValue | (1 << bucketIndex)) & 0xFF));

        return CompletableFuture.completedFuture(null);
    }
}
