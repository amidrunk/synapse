package io.synapse.data;

import io.synapse.*;
import io.synapse.csv.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InOrder;

import java.io.File;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class DeduplicatorTest {

    private final File tempDirectory = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());

    private final MessageMarshaller stringMarshaller = new MessageMarshaller() {
        @Override
        public byte[] marshal(Message<?> message) {
            return message.as(String.class).map(Message::getPayload).map(String::getBytes)
                    .orElseThrow(() -> new IllegalArgumentException("Unsupported message"));
        }

        @Override
        public Message<?> unmarshal(byte[] buffer) {
            return Message.of(new String(buffer));
        }
    };

    @Before
    public void setup() throws Exception {
        tempDirectory.mkdir();
    }

    @After
    public void tearDwon() throws Exception {
        FileUtils.forceDelete(tempDirectory);
    }

    @Test
    public void smallSetOfMessagesCanBeDeduplicated() {
        final Deduplicator deduplicator = Deduplicator
                .withMessageMarshaller(stringMarshaller)
                .until(m -> m.getPayload() == null)
                .withFileArea(tempDirectory.toPath())
                .andMaximumBlockSize(16384);

        final MessageContext messageContext = mock(MessageContext.class);

        deduplicator.processMessage(messageContext, Message.of("msg1"));
        deduplicator.processMessage(messageContext, Message.of("msg2"));
        deduplicator.processMessage(messageContext, Message.of("msg3"));
        deduplicator.processMessage(messageContext, Message.of("msg2"));
        deduplicator.processMessage(messageContext, Message.of("msg3"));
        deduplicator.processMessage(messageContext, Message.of("msg1"));
        deduplicator.processMessage(messageContext, Message.of("msg2"));
        deduplicator.processMessage(messageContext, Message.of("msg3"));
        deduplicator.processMessage(messageContext, Message.of("amsg2"));
        deduplicator.processMessage(messageContext, Message.of("amsg3"));
        deduplicator.processMessage(messageContext, Message.of("amsg1"));
        deduplicator.processMessage(messageContext, Message.of(null));

        verify(messageContext, times(1)).send(eq(Message.of("msg1")));
        verify(messageContext, times(1)).send(eq(Message.of("msg2")));
        verify(messageContext, times(1)).send(eq(Message.of("msg3")));
        verify(messageContext, times(1)).send(eq(Message.of("amsg1")));
        verify(messageContext, times(1)).send(eq(Message.of("amsg2")));
        verify(messageContext, times(1)).send(eq(Message.of("amsg3")));
        verify(messageContext, times(1)).send(eq(Message.of(null)));
        verifyNoMoreInteractions(messageContext);
    }

    @Test
    public void largeSetOfMessagesCanBeDeduplicated() {
        final Deduplicator deduplicator = Deduplicator
                .withMessageMarshaller(stringMarshaller)
                .until(m -> m.getPayload() == null)
                .withFileArea(tempDirectory.toPath())
                .andMaximumBlockSize(16384);

        final Set<String> result = new HashSet<>();

        final MessageContext context = new MessageContext() {

            @Override
            public void send(Message<?> message) {
                message.as(String.class).map(Message::getPayload).ifPresent(str -> {
                    if (!result.add(str)) {
                        fail("duplication of: \"" + str + "\"");
                    }
                });
            }

            @Override
            public void notify(Notification notification) {

            }
        };

        final int elements = 16384;

        for (final String string : generate(elements, 211342345L)) {
            deduplicator.processMessage(context, Message.of(string));
        }

        for (final String string : generate(elements, 934567127342L)) {
            deduplicator.processMessage(context, Message.of(string));
        }

        deduplicator.processMessage(context, Message.of(null));

        assertThat(result).hasSize(16384);
    }

    @Test
    @Ignore("more of a performance test")
    public void hugeDedupTest() throws InterruptedException {
        final Deduplicator deduplicator = Deduplicator
                .withMessageMarshaller(stringMarshaller)
                .until(m -> m.getPayload() == null)
                .withFileArea(tempDirectory.toPath())
                .andMaximumBlockSize(2097152);

        final Set<String> emailAddresses = new HashSet<>();
        final AtomicInteger totalProcessed = new AtomicInteger();

        final MessagingSystem messagingSystem = MessagingSystem.builder()
                .directlyTo(CsvProducer.byDefault())
                .directlyTo((ctx, m) -> {
                    if (m.getPayload() instanceof CsvRow) {
                        final CsvRow row = (CsvRow) m.getPayload();
                        final String email = row.getCells()[10];

                        if (!StringUtils.isEmpty(email)) {
                            totalProcessed.incrementAndGet();
                            ctx.send(Message.of(email));
                        }
                    } else if (m.getPayload() instanceof EndOfFile) {
                        ctx.send(Message.of(null));
                    }
                })
                .directlyTo(deduplicator)
                .directlyTo((ctx, m) -> {
                    if (m.getPayload() == null) {
                        System.out.println("totalProcessed: " + totalProcessed + ", emailAddresses=" + emailAddresses.size());
                    } else if (!emailAddresses.add((String) m.getPayload())) {
                        throw new IllegalArgumentException("Duplicate: " + m.getPayload());
                    }
                })
                .build();

        final MessageChannel channel = messagingSystem.newMessageChannel();

        channel.send(Message.of(ReadCsvFile.of(Paths.get("/Users/andnio/Playground/neo4j-dwh/in/customers_20092016.csv"), CsvFormat.byDefault())));

        Thread.sleep(120000);
    }

    private final String[] generate(int capacity, long seed) {
        final List<String> elements = new ArrayList<>(capacity);
        final Random random = new Random(seed);

        for (int i = 0; i < capacity; i++) {
            elements.add(String.valueOf(i));
        }

        final String[] result = new String[capacity];

        for (int i = 0; i < result.length; i++) {
            result[i] = elements.remove(random.nextInt(elements.size()));
        }

        return result;
    }
}