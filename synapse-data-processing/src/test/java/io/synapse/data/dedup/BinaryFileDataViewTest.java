package io.synapse.data.dedup;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.*;

public class BinaryFileDataViewTest {

    private File file;

    @Before
    public void setup() throws Exception {
        this.file = File.createTempFile("test", ".dat");
    }

    @After
    public void tearDown() {
        this.file.delete();
    }

    @Test
    public void smallBlockCanBeFlushedIntoFileView() throws Exception {
        final DataStore<byte[]> origin = new SortedUniqueInMemoryBinaryDataStore();

        origin.put("foo".getBytes());
        origin.put("bar".getBytes());
        origin.put("baz".getBytes());

        final BinaryFileDataView view = BinaryFileDataView.from(origin)
                .withFlushBufferSize(1024)
                .flushInto(file.toPath());

        try (final ViewStream<byte[]> stream = view.stream()) {
            final List<String> result = new LinkedList<>();

            while (stream.next()) {
                result.add(new String(stream.current()));
            }

            assertThat(result).containsExactly("bar", "baz", "foo");
        }
    }

    @Test
    public void largeBlockCanBeFlushedIntoFileView() throws Exception {
        final int elementCount = 10000;

        final DataView<byte[]> sourceDataView = () -> new ViewStream<byte[]>() {

            int current = -1;

            @Override
            public byte[] current() {
                if (current == -1) {
                    throw new NoSuchElementException();
                }

                return String.valueOf(current).getBytes(StandardCharsets.UTF_8);
            }

            @Override
            public boolean next() {
                return ++current < elementCount;
            }
        };

        final BinaryFileDataView view = BinaryFileDataView.from(sourceDataView)
                .withFlushBufferSize(1024)
                .flushInto(file.toPath());

        try (final ViewStream<byte[]> stream = view.stream()) {
            for (int i = 0; i < elementCount; i++) {
                assertThat(stream.next()).isTrue();
                final String actual = new String(stream.current(), StandardCharsets.UTF_8);
                assertThat(actual).isEqualTo(String.valueOf(i));
            }

            assertThat(stream.next()).isFalse();
        }
    }
}