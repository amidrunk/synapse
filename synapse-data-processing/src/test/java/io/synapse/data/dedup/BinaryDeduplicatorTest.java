package io.synapse.data.dedup;

import io.synapse.Message;
import io.synapse.MessageChannel;
import io.synapse.MessagingSystem;
import io.synapse.csv.*;
import io.synapse.exchange.Aggregator;
import io.synapse.exchange.Mailbox;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class BinaryDeduplicatorTest {

    private File tempDirectory;

    @Before
    public void setup() throws Exception {
        this.tempDirectory = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());
        this.tempDirectory.mkdirs();
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.forceDelete(tempDirectory);
    }

    @Test
    public void midSizedDataStreamCanBeDeduplicatedViaSortedBlocks() {
        final int elementCount = 16384;
        final BinaryDeduplicator deduplicator = BinaryDeduplicator.sortedBlocks()
                .setDataDirectory(tempDirectory.toPath())
                .setMaximumFileSize(16384)
                .build();

        shuffle(generateDataSet(elementCount), 1234523489723L).forEach(str -> deduplicator.put(str.getBytes(StandardCharsets.UTF_8)));
        shuffle(generateDataSet(elementCount), 9786345984234L).forEach(str -> deduplicator.put(str.getBytes(StandardCharsets.UTF_8)));

        final List<String> actual = new ArrayList<>(elementCount);

        try (final ViewStream<byte[]> stream = deduplicator.stream()) {
            while (stream.next()) {
                final byte[] current = stream.current();

                actual.add(new String(current, StandardCharsets.UTF_8));
            }
        }

        final List<String> expected = generateDataSet(elementCount);

        assertThat(actual.size()).isEqualTo(expected.size());

        for (final String string : expected) {
            assertThat(actual.remove(string)).isTrue();
        }
    }

    private List<String> generateDataSet(int count) {
        final ArrayList<String> result = new ArrayList<>(count);

        for (int i = 0; i < count; i++) {
            result.add(String.valueOf(i));
        }

        return result;
    }

    private List<String> shuffle(List<String> original, long seed) {
        final List<String> shuffled = new ArrayList<>(original.size());
        final Random random = new Random(seed);

        while (!original.isEmpty()) {
            shuffled.add(original.remove(random.nextInt(original.size())));
        }

        return shuffled;
    }

}