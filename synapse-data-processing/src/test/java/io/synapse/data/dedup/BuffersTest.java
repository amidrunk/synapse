package io.synapse.data.dedup;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class BuffersTest {

    @Test
    public void equalBuffersShouldHaveCompareValueZero() {
        assertThat(Buffers.compare("abc".getBytes(), "abc".getBytes())).isEqualTo(0);
    }

    @Test
    public void compareShouldReturnNegativeIfLeftPrecedesRight() {
        assertThat(Buffers.compare("a".getBytes(), "b".getBytes())).isNegative();
        assertThat(Buffers.compare("a".getBytes(), "bcd".getBytes())).isNegative();
        assertThat(Buffers.compare("abc".getBytes(), "bcd".getBytes())).isNegative();
        assertThat(Buffers.compare("abc".getBytes(), "abcd".getBytes())).isNegative();
    }

    @Test
    public void compareShouldReturnPositiveIfRightPrecedesLeft() {
        assertThat(Buffers.compare("b".getBytes(), "a".getBytes())).isGreaterThan(0);
        assertThat(Buffers.compare("b".getBytes(), "abc".getBytes())).isGreaterThan(0);
        assertThat(Buffers.compare("bcd".getBytes(), "abcde".getBytes())).isGreaterThan(0);
        assertThat(Buffers.compare("bcde".getBytes(), "bcd".getBytes())).isGreaterThan(0);
    }
}