package io.synapse.data.dedup;

import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.*;

public class SortedUniqueInMemoryBinaryDataStoreTest {

    private final SortedUniqueInMemoryBinaryDataStore block = new SortedUniqueInMemoryBinaryDataStore();

    @Test
    public void blockStreamCannotBeProgressedOnEmptyBlock() {
        assertThat(block.stream().next()).isFalse();
    }

    @Test
    public void bufferCannotBeRetrievedFromBlockStreamIfNoBufferIsAvailable() {
        assertThatThrownBy(() -> block.stream().current()).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void addedBufferCanBeStreamedThroughBlockStream() {
        assertThat(block.put("a".getBytes())).isTrue();

        final ViewStream<byte[]> stream = block.stream();

        assertThat(stream.next()).isTrue();
        assertThat(stream.current()).isEqualTo("a".getBytes());
        assertThat(stream.current()).isEqualTo("a".getBytes());
        assertThat(stream.next()).isFalse();
    }

    @Test
    public void multipleAddedBuffersShouldBeStreamedInCorrectSortOrder() {
        assertThat(block.put("abc".getBytes())).isTrue();
        assertThat(block.put("a".getBytes())).isTrue();
        assertThat(block.put("b".getBytes())).isTrue();
        assertThat(block.put("ab".getBytes())).isTrue();
        assertThat(block.put("bd".getBytes())).isTrue();
        assertThat(block.put("bce".getBytes())).isTrue();
        assertThat(block.put("bc".getBytes())).isTrue();

        assertThat(insertedElements()).containsExactly("a", "ab", "abc", "b", "bc", "bce", "bd");
    }

    @Test
    public void duplicateElementsShouldBeFilteredOutFromResult() {
        insert("abc", "a", "b", "ab", "ab", "bd", "bc", "bce", "bc", "abc", "b", "ab");

        assertThat(insertedElements()).containsExactly("a", "ab", "abc", "b", "bc", "bce", "bd");
    }

    private void insert(String ... strings) {
        Arrays.stream(strings).forEach(str -> block.put(str.getBytes(StandardCharsets.UTF_8)));
    }

    private List<String> insertedElements() {
        final ViewStream<byte[]> stream = block.stream();
        final List<String> result = new LinkedList<>();

        while (stream.next()) {
            result.add(new String(stream.current()));
        }
        return result;
    }
}