package io.synapse.data;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HashFunctionTest {

    @Test
    public void defaultHashFunctionShouldReturnObjectHashCode() {
        final int hashValue = HashFunction.defaultHashCode().hash(new Object() {
            @Override
            public int hashCode() {
                return 12345;
            }
        });

        assertThat(hashValue).isEqualTo(12345);
    }
}