package io.synapse.data;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

@Ignore("not implemented")
public class FileMemberFilterTest {

    private Path indexPath;

    private FileMemberFilter<String> fileMemberFilter;

    @Before
    public void setup() throws IOException {
        this.indexPath = Paths.get(System.getProperty("java.io.tmpdir"), "SYNAPSE-" + UUID.randomUUID().toString());
        this.fileMemberFilter = new FileMemberFilter<>(
                indexPath,
                8,
                HashFunction.defaultHashCode(),
                str -> str.getBytes(StandardCharsets.UTF_8)
        );

        Files.createDirectories(indexPath);
    }

    @After
    public void tearDown() throws IOException {
        Files.walkFileTree(indexPath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    @Test
    public void testAndAddShouldReturnDefiniteAnswers() throws Exception {
        assertThat(fileMemberFilter.testAndAdd("foo")).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(fileMemberFilter.testAndAdd("bar")).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(fileMemberFilter.testAndAdd("baz")).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(fileMemberFilter.testAndAdd("foo")).isEqualTo(MemberStatus.DEFINITELY);
        assertThat(fileMemberFilter.testAndAdd("bar")).isEqualTo(MemberStatus.DEFINITELY);
        assertThat(fileMemberFilter.testAndAdd("baz")).isEqualTo(MemberStatus.DEFINITELY);
    }
}