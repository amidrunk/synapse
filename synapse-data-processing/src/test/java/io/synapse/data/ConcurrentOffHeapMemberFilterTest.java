package io.synapse.data;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class ConcurrentOffHeapMemberFilterTest {

    private final MemberFilter<byte[]> filter = ConcurrentOffHeapMemberFilter.byDefault();

    @Test
    public void testShouldReturnDefinitelyNoForNonAddedBuffers() throws Exception {
        final List<byte[]> entries = IntStream.range(0, 100).mapToObj(i -> "string_" + i).map(String::getBytes).collect(Collectors.toList());

        for (final byte[] entry : entries) {
            final MemberStatus status = filter.test(entry).toCompletableFuture().get();

            assertThat(status).isEqualTo(MemberStatus.DEFINITELY_NOT);
        }
    }

    @Test
    public void testShouldReturnDefinitelyYesForAddedBuffers() throws Exception {
        final List<byte[]> entries = IntStream.range(0, 100)
                .mapToObj(i -> "string_" + i)
                .map(String::getBytes)
                .collect(Collectors.toList());

        entries.forEach(filter::add);

        for (final byte[] buf : entries) {
            final MemberStatus status = filter.test(buf).toCompletableFuture().get();

            assertThat(status).isEqualTo(MemberStatus.DEFINITELY);
        }
    }

    @Test
    public void tooLargeBufferCannotBeAdded() throws Exception {
        final ConcurrentOffHeapMemberFilter filter = ConcurrentOffHeapMemberFilter.builder()
                .setBlockSize(32)
                .build();

        try {
            filter.add(new byte[64]).toCompletableFuture().get();
            fail("expected exception");
        } catch (ExecutionException e) {
            assertThat(e.getCause()).isInstanceOf(IllegalArgumentException.class);
        }
    }
}