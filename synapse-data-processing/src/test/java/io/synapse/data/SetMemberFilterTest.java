package io.synapse.data;

import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

@Ignore("not implemented")
public class SetMemberFilterTest {

    private final SetMemberFilter<String> filter = new SetMemberFilter<>(HashFunction.defaultHashCode());

    @Test
    public void testAndAddShouldGiveDefiniteMemberStatuses() {
        assertThat(filter.testAndAdd("foo")).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("bar")).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("baz")).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("foo")).isEqualTo(MemberStatus.DEFINITELY);
        assertThat(filter.testAndAdd("bar")).isEqualTo(MemberStatus.DEFINITELY);
        assertThat(filter.testAndAdd("baz")).isEqualTo(MemberStatus.DEFINITELY);
    }

    @Test
    public void testShouldReturnDefinitelyNotIfObjectIsNotAMember() {
        assertThat(filter.test("foo")).isEqualTo(MemberStatus.DEFINITELY_NOT);
    }

    @Test
    public void testShouldReturnDefinitelyIfObjectIsAMember() {
        filter.add("foo");
        assertThat(filter.test("foo")).isEqualTo(MemberStatus.DEFINITELY);
    }
}