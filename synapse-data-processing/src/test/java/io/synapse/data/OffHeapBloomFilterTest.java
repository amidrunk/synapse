package io.synapse.data;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class OffHeapBloomFilterTest {

    private final BloomFilter<String> filter = new OffHeapBloomFilter<>(HashFunction.defaultHashCode(), 4096);

    @Test
    public void testShouldReturnDefinitelyNotIfObjectHasNotBeenTestedBefore() throws Exception {
        assertThat(filter.testAndAdd("foo").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("bar").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("baz").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
    }

    @Test
    public void testShouldReturnMaybeIfObjectHasBeenTestedBefore() throws Exception {
        assertThat(filter.testAndAdd("foo").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("bar").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("baz").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.testAndAdd("foo").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
        assertThat(filter.testAndAdd("bar").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
        assertThat(filter.testAndAdd("baz").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
    }

    @Test
    public void objectShouldDefinitelyNotBeInSetIfItHasNotBeenAdded() throws Exception {
        assertThat(filter.test("foo").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.test("bar").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.test("baz").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
    }

    @Test
    public void objectShouldMaybeBeInSetIfItHasBeenAdded() throws Exception {
        filter.add("foo");
        assertThat(filter.test("foo").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
        assertThat(filter.test("bar").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);
        assertThat(filter.test("baz").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);

        filter.add("bar");
        assertThat(filter.test("foo").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
        assertThat(filter.test("bar").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
        assertThat(filter.test("baz").toCompletableFuture().get()).isEqualTo(MemberStatus.DEFINITELY_NOT);

        filter.add("baz");
        assertThat(filter.test("foo").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
        assertThat(filter.test("bar").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
        assertThat(filter.test("baz").toCompletableFuture().get()).isEqualTo(MemberStatus.POSSIBLY);
    }
}