package io.synapse.data;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;

@Ignore("not implemented")
public class CachedFileDataStoreTest {

    private Path path;

    private CachedFileBlock block;

    @Before
    public void setup() throws Exception {
        this.path = Paths.get(System.getProperty("java.io.tmpdir"), "SYNAPSE-" + UUID.randomUUID().toString());
        this.block = new CachedFileBlock(path, () -> new SetMemberFilter<>(Arrays::hashCode));
    }

    @After
    public void tearDown() throws Exception {
        Files.delete(path);
    }

    @Test
    public void writtenEntriesCanBeRetrieved() {
        final byte[] entry1 = "foo".getBytes(StandardCharsets.UTF_8);
        final byte[] entry2 = "foo".getBytes(StandardCharsets.UTF_8);
        final byte[] entry3 = "foo".getBytes(StandardCharsets.UTF_8);

        block.add(entry1);
        block.add(entry2);
        block.add(entry3);

        assertThat(block.test(entry1)).isEqualTo(MemberStatus.DEFINITELY);
        assertThat(block.test(entry2)).isEqualTo(MemberStatus.DEFINITELY);
        assertThat(block.test(entry3)).isEqualTo(MemberStatus.DEFINITELY);
    }

    @Test
    public void canBeFlushedAndReloaded() throws Exception {
        final byte[] entry1 = "foo".getBytes(StandardCharsets.UTF_8);
        final byte[] entry2 = "foo".getBytes(StandardCharsets.UTF_8);
        final byte[] entry3 = "foo".getBytes(StandardCharsets.UTF_8);

        block.add(entry1);
        block.add(entry2);
        block.add(entry3);
        block.flush().toCompletableFuture().get();

        System.out.println("OK!");
    }

}